"use strict";

var error_message = "Oops! Something went wrong. Let's and try that again";

function bootstrapNotify(message, type){
    $.notify({
        message: message
    }, {
        type: type
    });
}

function bootstrapAlert($message, $type){
    if($type === undefined) $type = 'danger';

    return '<div class="alert alert-'+ $type+ ' alert-rounded">'+ $message+ '<button type="button" class="close"' +
        ' data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>';
}


function check_all(a){
    $('.chk:visible').prop('checked', $(a).is(':checked'));
}


function submitForm(form_id, silent){
    if(silent === undefined) silent = false;

    var form = $(form_id);
    var form_data = form.serializeArray();

    $('#preloader').show();

    var url = form.attr('action');

    return $.post(url, form_data, function (result) {
        if(!silent) bootstrapNotify(result.message, result.status ? 'success' : 'danger');
    }, 'json').fail(function () {

        if(!silent) bootstrapNotify(error_message, 'danger');
    }).always(function(){
        $('#preloader').hide();
    });
}


function submitForm2(form_id, silent){
    if(silent === undefined) silent = false;

    var form  = $(form_id);
    var form_data = new FormData(form[0]);
    $('#preloader').show();

    return $.ajax({
        url:		form.attr('action'),
        type:		'POST',
        data:		form_data,
        contentType: false,
        processData: false,
        dataType:	'json',
        cache:		false,
        success: function (result, status) {
            if(!silent) bootstrapNotify(result.message, result.status ? 'success' : 'danger');
        },
        error: function () {
            if(!silent) bootstrapNotify(error_message, 'danger');
        },
        complete: function(){
            $('#preloader').hide();
        }
    });
}


function getRequest(url, silent){
    if(silent === undefined) silent = false;

    $('#preloader').show();

    return $.get(url, function (result) {
        if(!silent) bootstrapNotify(result.message, result.status ? 'success' : 'danger');
    }, 'json').fail(function (result) {
        bootstrapNotify(error_message, 'danger');
    }).always(function(){
        $('#preloader').hide();
    });
}