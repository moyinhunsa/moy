<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title> Admin Dashboard</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Themesbrand" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App Icons -->
        <link rel="shortcut icon" href="<?=base_url("assets/admin/images/favicon.icon")?>">

        <!-- Basic Css files -->
        <link href="<?=base_url("assets/admin/css/bootstrap.min.css")?>" rel="stylesheet" type="text/css">
        <link href="<?=base_url("assets/admin/css/icons.css")?>" rel="stylesheet" type="text/css">
        <link href="<?=base_url("assets/admin/css/style.css")?>" rel="stylesheet" type="text/css">
        <link href="<?=base_url("assets/admin/css/animate.css")?>" rel="stylesheet" type="text/css">
    </head>
    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader" style="opacity: 0.8">
            <div id="status">
                <div class="sk-three-bounce">
                    <div class="sk-child sk-bounce1"></div>
                    <div class="sk-child sk-bounce2"></div>
                    <div class="sk-child sk-bounce3"></div>
                </div>
            </div>
        </div>

        <!-- Begin page -->
        <div class="accountbg"></div>
        <div class="wrapper-page">

            <div class="card">
                <div class="card-body">
                    <div class="p-3">
                        <h4 class="text-muted font-18 m-b-5 text-center">Welcome Back !</h4>
                        <p class="text-muted text-center">Sign in to admin.</p>

                               <form class="form-horizontal m-t-30" action="login/auth" id="form-login" method="post">

                            <div class="form-group">
                                <label for="username">Email</label>
                                <input type="text" class="form-control" id="username" placeholder="Enter username" name="email">
                            </div>

                            <div class="form-group">
                                <label for="userpassword">Password</label>
                                <input type="password" class="form-control" id="userpassword" placeholder="Enter password" name="password">
                            </div>

                            <div class="form-group row m-t-20">
                                <div class="col-sm-6">
                                    <div class="custom-control mt-2 custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">Remember me</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <button class="btn btn-success w-md waves-effect waves-light" type="submit">Log In</button>
                                </div>
                            </div>

                            <div class="form-group m-t-10 mb-0 row">
                                <div class="col-12 m-t-20">
                                    <a href="pages-recoverpw.html" class="text-muted"><i class="mdi mdi-lock"></i> Forgot your password?</a>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
                <p class="">Don't have an account ? <a href="pages-register.html" class="font-500 font-14 text-dark font-secondary"> Signup Now </a> </p>
                <p class="">© 2020 OxfordRealAsset. Crafted with <i class="mdi mdi-heart text-danger"></i> by MO</p>
            </div>

        </div>
        <!-- jQuery  -->
        <script src="<?=base_url("assets/admin/js/jquery.min.js")?>"></script>
        <script src="<?=base_url("assets/admin/js/popper.min.js")?>"></script>
        <script src="<?=base_url("assets/admin/js/bootstrap.min.js")?>"></script>
        <script src="<?=base_url("assets/admin/js/modernizr.min.js")?>"></script>
        <script src="<?=base_url("assets/admin/js/jquery.slimscroll.js")?>"></script>
        <script src="<?=base_url("assets/admin/js/waves.js")?>"></script>
        <script src="<?=base_url("assets/admin/js/jquery.nicescroll.js")?>"></script>
        <script src="<?=base_url("assets/admin/js/jquery.scrollTo.min.js")?>"></script>

        <!-- App js -->
        <script src="<?=base_url("assets/admin/js/app.js")?>"></script>
        <script src="<?=base_url("assets/admin/js/bootstrap-notify.js")?>"></script>
        <script src="<?=base_url("assets/admin/js/mo.js")?>"></script>


        <script type="text/javascript">
           
           $('#form-login').submit(function(e){
                e.preventDefault();

                var xhr = submitForm('#form-login');
                xhr.done(function(result){
                    if(result.status){
                        window.location.href = result.data.goto;
                    }
                })
           });

        </script>

    </body>
</html>