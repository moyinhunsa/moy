<!-- footer -->
<footer class="footer-bg footer-p pt-100 pb-80 ">
  
  <div class="footer-top pb-30">
      <div class="container">
          <div class="row justify-content-between">
              <div class="col-xl-6 col-lg-6 col-sm-6">
                  <div class="footer-widget mb-30">
                      <!-- <div class="logo mb-35">
                          <a href="#"><img src="<?=base_url("assets/img/logo/{$site_info['site_logo']}")?>" alt="logo" style="width:15%"></a>
                      </div> -->
                      <div class="footer-text mb-20">
                          <p><?= $site_info['footer_line']?></p>
                      </div>
                      <div class="footer-social">
                          <span>Follow Us</span>
                          <a href="#"><i class="fab fa-facebook-f"></i></a>
                          <a href="#"><i class="fab fa-twitter"></i></a>
                          <a href="#"><i class="fab fa-instagram"></i></a>
                          <a href="#"><i class="fab fa-google-plus-g"></i></a>
                      </div>
                  </div>
              </div>
              <div class="col-xl-2 col-lg-3 col-sm-6">
                  <div class="footer-widget mb-30">
                      <div class="f-widget-title">
                          <h5>Company News</h5>
                      </div>
                      <div class="footer-link">
                          <ul>                                        
                              <li><a href="#">Partners</a></li>
                              <li><a href="<?=base_url("about.php")?>">About Us</a></li>
                              <li><a href="#">Career</a></li>
                              <li><a href="#">Reviews</a></li>
                              <li><a href="#">Terms & Conditions</a></li>
                              <li><a href="#">Help</a></li>
                          </ul>
                      </div>
                  </div>
              </div>
              <div class="col-xl-2 col-lg-3 col-sm-6">
                  <div class="footer-widget mb-30">
                      <div class="f-widget-title">
                          <h5>Useful Links</h5>
                      </div>
                      <div class="footer-link">
                          <ul>
                              <li><a href="#">Home</a></li>
                              <li><a href="#">About Us</a></li>
                              <li><a href="#">Services</a></li>
                              <li><a href="#">Project</a></li>
                              <li><a href="#">Our Team</a></li>
                              <li><a href="#">Latest Blog</a></li>
                          </ul>
                      </div>
                  </div>
              </div>                     
              
          </div>
      </div>
  </div>
  <div class="copyright-wrap">
      <div class="container">
          <div class="row">
              <div class="col-12">
                  <div class="copyright-text text-center">
                      <p>&copy; 2020 @ OxfordRealAsset.  All Rights Reserved.</p>
                  </div>
              </div>
          </div>
      </div>
  </div>
</footer>
<!-- footer-end -->


<!-- JS here -->
<script src="<?=base_url("assets/js/vendor/modernizr-3.5.0.min.js")?>"></script>
<script src="<?=base_url("assets/js/vendor/jquery-1.12.4.min.js")?>"></script>
<script src="<?=base_url("assets/js/popper.min.js")?>"></script>
<script src="<?=base_url("assets/js/bootstrap.min.js")?>"></script>
<script src="<?=base_url("assets/js/one-page-nav-min.js")?>"></script>
<script src="<?=base_url("assets/js/slick.min.js")?>"></script>
<script src="<?=base_url("assets/js/ajax-form.js")?>"></script>
<script src="<?=base_url("assets/js/paroller.js")?>"></script>
<script src="<?=base_url("assets/js/wow.min.js")?>"></script>
<script src="<?=base_url("assets/js/js_isotope.pkgd.min.js")?>"></script>
<script src="<?=base_url("assets/js/imagesloaded.min.js")?>"></script>
<script src="<?=base_url("assets/js/parallax.min.js")?>"></script>
<script src="<?=base_url("assets/js/jquery.waypoints.min.js")?>"></script>
<script src="<?=base_url("assets/js/jquery.counterup.min.js")?>"></script>
<script src="<?=base_url("assets/js/jquery.scrollUp.min.js")?>"></script>
<script src="<?=base_url("assets/js/parallax-scroll.html")?>"></script>
<script src="<?=base_url("assets/js/jquery.magnific-popup.min.js")?>"></script>
<script src="<?=base_url("assets/js/element-in-view.js")?>"></script>
<script src="<?=base_url("assets/js/main.js")?>"></script>
