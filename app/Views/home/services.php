
        <main>
            <!-- breadcrumb-area -->
            <section class="breadcrumb-area d-flex align-items-center" style="background-image:url(img/testimonial/test-bg.jpg)">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2">
                            <div class="breadcrumb-wrap text-center">
                                <div class="breadcrumb-title mb-30">
                                    <h2>Services</h2>                                    
                                </div>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Services</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- breadcrumb-area-end -->
			<!-- services-area -->
            <section id="services" class="services-area services-bg services-two pt-120 pb-90">
                <div class="container">             
                    <div class="row">
					
						<div class="col-lg-4 col-md-6 mb-30">
                            <div class="s-single-services wow fadeInUp  animated" data-animation="fadeInDown animated" data-delay=".2s" style="visibility: visible; animation-name: fadeInUp;">
                                <div class="services-ico2">
                                 <i class="fad fa-megaphone"></i>
                                </div>
                                <div class="second-services-content2">
                                    <h5>Dedicated Production & Planning Teams </h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor inc ididunt ut labore. </p>
									<a href="#">Read More</a>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-4 col-md-6 mb-30">
                            <div class="s-single-services wow fadeInUp  animated" data-animation="fadeInDown animated" data-delay=".2s" style="visibility: visible; animation-name: fadeInUp;">
                                <div class="services-ico2">
                                   <i class="far fa-building"></i>
                                </div>
                                <div class="second-services-content2">
                                    <h5>We’re Here To Make You Successful</h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor inc ididunt ut labore. </p>
									<a href="#">Read More</a>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-4 col-md-6 mb-30">
                            <div class="s-single-services wow fadeInUp  animated" data-animation="fadeInDown animated" data-delay=".2s" style="visibility: visible; animation-name: fadeInUp;">
                                <div class="services-ico2">
                                  <i class="fad fa-home"></i>
                                </div>
                                <div class="second-services-content2">
                                    <h5>Premium, Fresh Cuisine Made Onsite</h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor inc ididunt ut labore. </p>
									<a href="#">Read More</a>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-4 col-md-6 mb-30">
                            <div class="s-single-services wow fadeInUp  animated" data-animation="fadeInDown animated" data-delay=".2s" style="visibility: visible; animation-name: fadeInUp;">
                                <div class="services-ico2">
                                 <i class="fas fa-search-location"></i>
                                </div>
                                <div class="second-services-content2">
                                    <h5>Flexible, Contemporary & Included Spaces </h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor inc ididunt ut labore. </p>
									<a href="#">Read More</a>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-4 col-md-6 mb-30">
                            <div class="s-single-services wow fadeInUp  animated" data-animation="fadeInDown animated" data-delay=".2s" style="visibility: visible; animation-name: fadeInUp;">
                                <div class="services-ico2">
                                   <i class="far fa-car-garage"></i>
                                </div>
                                <div class="second-services-content2">
                                    <h5>Human-Centered Design Friendly Spaces</h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor inc ididunt ut labore. </p>
									<a href="#">Read More</a>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-4 col-md-6 mb-30">
                            <div class="s-single-services wow fadeInUp  animated" data-animation="fadeInDown animated" data-delay=".2s" style="visibility: visible; animation-name: fadeInUp;">
                                <div class="services-ico2">
                                  <i class="fas fa-comments-alt"></i>
                                </div>
                                <div class="second-services-content2">
                                    <h5>Broker Cutting-Edge And Technology</h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor inc ididunt ut labore. </p>
									<a href="#">Read More</a>
                                </div>
                            </div>
                        </div>
                      
                    </div>
                    
                </div>
            </section>
            <!-- services-area-end -->
						
			<!-- cta-area -->
            <section class="cta-area cta-bg pt-120 pb-120" style="background-image:url(img/bg/cta_bg02.jpg)">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-lg-12">
                            <div class="section-title cta-title wow fadeInLeft animated" data-animation="fadeInDown animated" data-delay=".2s">
								<p>Don’t hestitate to contact us</p>
                                <h2>MAKE AN APPOINTMENT NOW</h2>
								<h3>666 888 0000</h3>
								 <div class="cta-btn s-cta-btn wow fadeInRight animated mt-30" data-animation="fadeInDown animated" data-delay=".2s">
									<a href="#" class="btn">Contact Us</a>
								</div>
                            </div>
                                             
                        </div>
					
                    </div>
                </div>
            </section>
            <!-- cta-area-end -->	
			
			
			<!-- services-area -->
            <section id="services" class="services-area services-bg services-two pt-120 pb-90">
                <div class="container">                   
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="s-single-services  wow fadeInUp animated" data-animation="fadeInDown animated" data-delay=".2s">
                                <div class="second-services-content">
                                    <h5>Air Conditioning</h5>                                    
                                </div>
								<div class="services-icon">
                                   <i class="far fa-star-half"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="s-single-services active wow fadeInUp animated" data-animation="fadeInDown animated" data-delay=".2s">
                                <div class="second-services-content">
                                    <h5>Concrete Flooring </h5>                                    
                                </div>
								<div class="services-icon">
                                   <i class="far fa-star-half"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="s-single-services wow fadeInUp animated" data-animation="fadeInDown animated" data-delay=".2s">
                                <div class="second-services-content">
                                    <h5>Roof Terrace </h5>                                    
                                </div>
								<div class="services-icon">
                                   <i class="far fa-star-half"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="s-single-services wow fadeInUp animated" data-animation="fadeInDown animated" data-delay=".2s">
                                <div class="second-services-content">
                                    <h5>Bedding</h5>                                    
                                </div>
								<div class="services-icon">
                                   <i class="far fa-star-half"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="s-single-services wow fadeInUp animated" data-animation="fadeInDown animated" data-delay=".2s">
                                <div class="second-services-content">
                                    <h5>Coffee Pot </h5>                                    
                                </div>
								<div class="services-icon">
                                   <i class="far fa-star-half"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="s-single-services wow fadeInUp animated" data-animation="fadeInDown animated" data-delay=".2s">
                                <div class="second-services-content">
                                    <h5>Outdoor Kitchen </h5>                                    
                                </div>
								<div class="services-icon">
                                   <i class="far fa-star-half"></i>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-4 col-md-6">
                            <div class="s-single-services wow fadeInUp animated" data-animation="fadeInDown animated" data-delay=".2s">
                                <div class="second-services-content">
                                    <h5>Balcony</h5>                                    
                                </div>
								<div class="services-icon">
                                   <i class="far fa-star-half"></i>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-4 col-md-6">
                            <div class="s-single-services wow fadeInUp animated" data-animation="fadeInDown animated" data-delay=".2s">
                                <div class="second-services-content">
                                    <h5>Microwave</h5>                                    
                                </div>
								<div class="services-icon">
                                   <i class="far fa-star-half"></i>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-4 col-md-6">
                            <div class="s-single-services wow fadeInUp animated" data-animation="fadeInDown animated" data-delay=".2s">
                                <div class="second-services-content">
                                    <h5>Terrace</h5>                                    
                                </div>
								<div class="services-icon">
                                   <i class="far fa-star-half"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </section>
            <!-- services-area-end -->
        </main>
        <!-- main-area-end -->
       