<!doctype html>
<html class="no-js" lang="zxx">
    
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>OxfordRealAsset</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    <!-- Place favicon.ico in the root directory -->

	<!-- CSS here -->
    <link rel="stylesheet" href="<?=base_url("assets/css/bootstrap.min.css")?>">
    <link rel="stylesheet" href="<?=base_url("assets/css/animate.min.css")?>">
    <link rel="stylesheet" href="<?=base_url("assets/css/magnific-popup.css")?>">
    <link rel="stylesheet" href="<?=base_url("assets/fontawesome/css/all.min.css")?>">
    <link rel="stylesheet" href="<?=base_url("assets/css/dripicons.css")?>">
    <link rel="stylesheet" href="<?=base_url("assets/css/slick.css")?>">
    <link rel="stylesheet" href="<?=base_url("assets/css/default.css")?>">
     <link rel="stylesheet" href="<?=base_url("assets/css/style.css")?>"> 
     <link rel="stylesheet" href="<?=base_url("assets/css/responsive.css")?>">
</head>
    <body>
        <!-- header -->
        <header >  	
            <div id="header-sticky" style="background-image: linear-gradient(-90deg, rgb(0,0,0), rgb(148, 7, 82), rgb(10, 44, 107), rgba(0,0,0,0.2), rgba(0,0,0,0));">
                <div class="container">
                    <div class="second-menu">
                        <div class="row align-items-center">
                            <div style="width:15%; margin-left:5px; " >
                                    <a href="home.php"><img src="<?= base_url("assets/img/logo/{$site_info['site_logo']}") ?>" alt="logo" width="50%"></a>
                            </div> 
                            <div class="col-xl-10 col-lg-9">
                                <div class="responsive"><i class="icon dripicons-align-right"></i></div>
                                <div class="main-menu text-right <text-xl-right></text-xl-right>">
                                    <nav id="mobile-menu">
                                        <ul>
                                            <li  class="navbar">
												<a href="<?=base_url("index.php")?>">Home</a>
                                            <li><a href="<?=base_url("home/about")?>">About Us</a></li>
                                            <li><a href="<?=base_url("home/services")?>">Services</a></li>
                                            <li><a href="<?=base_url("home/apartments")?>">Apartments </a></li>                              
											<li><a href="<?=base_url("home/blog")?>">Blog</a></li>
                                            <li><a href="<?=base_url("home/contact")?>">Contact</a></li>                                               
                                            <li><a href="<?=base_url("login")?>" class="top-btn">ADMIN</a></li>                                               
                                        </ul>
                                    </nav>
                                </div>
                            </div>                           
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- header-end -->
        <!-- main-area -->