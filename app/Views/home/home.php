
        <main>
            <!-- slider-area -->
            <section id="home" class="slider-area fix p-relative">
                <div class="slider-active">
                    <?php
                    foreach ($banners as $b) { ?>

                        <div class="single-slider slider-bg d-flex align-items-center" style="background-image:url(<?= bannerImageSrc($b['banner_img'])?>)">
                            <div class="container">
                                <div class="row">                           
                                    <div class="col-lg-8">
                                        <div class="slider-content s-slider-content text-left">
                                            <h2 data-animation="fadeInUp" data-delay=".4s"><?= $b['banner_title']?></h2> 
                                            <div class="slider-btn mt-55">                                          
                                                <a href="#" class="btn ss-btn" data-animation="fadeInRight" data-delay=".8s">Get a Quote</a>
                                                <a href="https://www.youtube.com/watch?v=vKSA_idPZkc" class="video-i popup-video" data-animation="fadeInUp" data-delay=".8s" style="animation-delay: 0.8s;" tabindex="0"><i class="fas fa-play"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="slider-price">
                                            <h3>Price:</h3>
                                            <h2><?= $b['banner_price']?><h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                    }
                    ?>
                </div>
            </section>
            <!-- slider-area-end -->
			<!-- about-area -->
            <section id="about" class="about-area about-p pt-120 pb-120 p-relative">
                <div class="container">
                    <div class="row ">
					<div class="col">
                            <div class="s-about-img p-relative">
                                <img src="<?=base_url("assets/img/features/{$about_us['about_img']}")?>" alt="img" style="max-width:100%;">
                                <div class="about-text second-about">
                                    <span><?= $about_us['experience']?> years of <br> experience</span>
                                </div>
                            </div>
                        </div>
					<div class="col">
                            <div class="about-content s-about-content pl-30">
                                <div class="about-title second-atitle">
                                    <span>About Us</span>
                                    <h2><?= $about_us['title']?></h2>
                                    <p><span></span><?= $about_us['short_desc']?></p>
                                </div>
                                <p><?= nl2br($about_us['long_desc'])?></p>
                                <a href="#" class="btn">Get Started</a>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </section>
            <!-- about-area-end -->
			<!-- services-area -->
            <section id="services" class="services-area services-bg services-two pt-120 pb-90">
                <div class="container">
                      <div class="row justify-content-center">
                      <div class="col-xl-8 col-lg-10">
                            <div class="section-title text-center pl-40 pr-40 mb-80 wow fadeInDown animated" data-animation="fadeInDown animated" data-delay=".2s">
                                <span>our services</span>
                                <h2>What We Do </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
					<?php
                    foreach ($services as $s) { ?>
                        <div class="col-lg-4 col-md-6 mb-30">
                            <div class="s-single-services wow fadeInUp  animated" data-animation="fadeInDown animated" data-delay=".2s" style="visibility: visible; animation-name: fadeInUp;">
                                <div class="services-ico2">
                                 <i class="fad fa-megaphone"></i>
                                </div>
                                <div class="second-services-content2">
                                    <h5><?= $s['service_title']?> </h5>
                                    <p> <?= $s['service_desc']?></p>
                                    <a href="#">Read More</a>
                                </div>
                            </div>
                        </div>
                     <?php
                    }
                    ?>
                    </div>
                    
                </div>
            </section>
            <!-- services-area-end -->
			<!-- cta-area -->
            <section class="cta-area cta-bg pt-120 pb-120" style="background-image:url(assets/img/bg/cta_bg02.jpg)">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-lg-12">
                            <div class="section-title cta-title wow fadeInLeft animated" data-animation="fadeInDown animated" data-delay=".2s">
								<h4><p>Don’t hestitate to contact us</p></h4>
								<h3><?= $site_info['site_phone']?></h3>
								 <div class="cta-btn s-cta-btn wow fadeInRight animated mt-30" data-animation="fadeInDown animated" data-delay=".2s">
									<a href="#" class="btn">Contact Us</a>
								</div>
                            </div>
                                             
                        </div>
					
                    </div>
                </div>
            </section>
            <!-- cta-area-end -->			
			
			<!-- apartments-area -->
            <section class="apartments pt-120 pb-90" style="background:#f5f5f5;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-8 col-lg-10">
                            <div class="section-title text-center pl-40 pr-40 mb-80 wow fadeInDown animated" data-animation="fadeInDown animated" data-delay=".2s">
                                <span>Our Plans</span>
                                <h2> Select Availability</h2>
                            </div>
                        </div>
                    </div>

              <div class="row">
			 
				   <div class="col-lg-12 ">
                  <div class="availability-section">
			<div class="availability-table">
				<table class="table text-center">
					<thead>
						<tr>
							<th scope="col">residence</th>
							<th scope="col">bed/bath</th>
							<th scope="col">SQ.FT</th>
							<th scope="col">sale price</th>
							<th scope="col">rent price</th>
							<th scope="col">floor plan</th>
						</tr>
					</thead>
					<tbody>
						
                        <?php
                            foreach ($plans as $pl) { ?>
                                <tr>
                                    <td class="name"><?= $pl['residence']
                                    ?></td>
                                    <td><?= $pl['bed_bath']
                                    ?></td>
                                    <td>700</td>
                                    <td class="price">$<?= $pl['sale_price']
                                    ?></td>
                                    <td class="price">$<?= $pl['rent_price']
                                    ?>/m</td>
                                    <td>
                                        <a class="btn popup-image" href="assets/img/<?= $pl['plan_img']?>" data-elementor-open-lightbox="yes">view now </a>
                                    </td>

                                </tr>
                                <?php
                            }
                        ?>
					</tbody>
				</table>
			</div>
		</div>
                
                </div>
              </div>
        </div>
      </div>

                    
                </div>
            </section>
            <!-- apartments-area-end -->
		
			
            <!-- testimonial-area -->
            <section id="testimonios" class="testimonial-area gray-bg testimonial-p pt-115 pb-185 text-center" style="background-image:url(assets/img/testimonial/test-bg.jpg); background-repeat:no-repeat">
                <div class="container">
                    <div class="row">
                          <div class="col-lg-2">
						  </div>
                        <div class="col-lg-8">
                            <div class="section-title center-align mb-40 wow fadeInDown animated" data-animation="fadeInDown animated" data-delay=".2s">
                                <span>See what Clients have to say about Us</span>
                                <h2>Testimonials</h2>
                            </div>
                            <div class="testimonial-active">
                                <?php
                                foreach ($testimony as $t) { ?>
                                   <div class="single-testimonial">
                                        <i class="fas fa-quote-left"></i>
                                        <p>“<?= $t['testimony']?></p>
                                        <div class="testi-author text-center">
                                            <img class= "rounded-circle" src="<?= clientImageSrc($t['client_img'])?>" alt="img" width="100px;">
                                            <div class="ta-info">
                                                <h6><?= $t['client_name']?></h6>
                                                <span><?= $t['client_desc']?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                             
                            </div>
                        </div>
						   <div class="col-lg-2">
						  </div>
                    </div>
                </div>
            </section>
            <!-- testimonial-area-end -->
           
           
            <!-- blog-area -->
            <section id="blog" class="blog-area  p-relative pt-120 pb-90">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-8 col-lg-10">
                            <div class="section-title text-center mb-80 wow fadeInDown animated" data-animation="fadeInDown animated" data-delay=".2s">
                                <span>New Every Day</span>
                                <h2>Latest News</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php
                        foreach ($posts as $po) { ?>
                            <div class="col-lg-4 col-md-6">
                                <div class="single-post mb-30 wow fadeInUp animated" data-animation="fadeInDown animated" data-delay=".2s">
                                    <div class="blog-thumb">
                                        <a href="blog-details.html"><img src="<?=base_url("assets/img/blog/{$po['post_img']}")?>" alt="img"></a>
                                    </div>
                                    <div class="blog-content">
                                        <div class="b-meta mb-20">
                                            <ul>
                                                <li><a href="#">By <?= $po['fullname']?> .</a></li>
                                                <li><a href="#">5 Dec 2019 .</a></li>
                                                <li><a href="#" class="corpo">Real Estate </a></li>
                                            </ul>
                                        </div>
                                        <h4><a href="blog-details.html"><?= $po['post_title']?></a></h4> 
                                        <a href="#"><?= $po['post_details']?></a>                                   
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                       
                    </div>
                </div>
            </section>
            <!-- blog-area-end -->
			
            <!-- contact-area -->
            <section id="contact" class="contact-area contact-bg pt-120 pb-120 p-relative fix" style="background-image:url(assets/img/bg/contact_bg.jpg)">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-7 col-lg-8">
                            <div class="section-title text-center mb-80 wow fadeInDown animated" data-animation="fadeInDown animated" data-delay=".2s">
                                <span>Contact</span>
                                <h2>Get In Touch</h2>
                            </div>
                        </div>
                    </div>
					<div class="row">
						<div class="col-lg-4">
						<div class="contact-info">
						<div class="single-cta pb-30 mb-30 wow fadeInUp animated" data-animation="fadeInDown animated" data-delay=".2s">
                                <div class="f-cta-icon">
                                    <i class="far fa-map"></i>
                                </div>
                                <h5>Office Address</h5>
                                <p><?= $site_info['site_address']?></p>
                            </div>
							 <div class="single-cta pb-30 mb-30 wow fadeInUp animated" data-animation="fadeInDown animated" data-delay=".2s">
                                <div class="f-cta-icon">
                                    <i class="far fa-clock"></i>
                                </div>
                                <h5>Working Hours</h5>
                                <p><?= $site_info['site_working_hours']?></p>
                            </div>
							 <div class="single-cta pb-30 mb-30 wow fadeInUp animated" data-animation="fadeInDown animated" data-delay=".2s">
                                <div class="f-cta-icon">
                                    <i class="far fa-envelope-open"></i>
                                </div>
                                <h5>Message Us</h5>
                                <p>We are always with you to solve your problem
                                    mail us : <a href="#"><?= $site_info['site_email']?></a></p>
                            </div>
                            </div>
							
						</div>
						<div class="col-lg-8">
						<form action="#" class="contact-form wow fadeInUp animated" data-animation="fadeInDown animated" data-delay=".2s">
							<div class="row">
                            <div class="col-lg-12">
                                <div class="contact-field p-relative c-name mb-40">                                    
                                    <input type="text" placeholder="Write here  Jhonathan Doe">
                                </div>                               
                            </div>
							<div class="col-lg-12">                               
                                <div class="contact-field p-relative c-email mb-40">                                    
                                    <input type="text" placeholder="Write here youremail">
                                </div>                                
                            </div>
							<div class="col-lg-12">                               
                                <div class="contact-field p-relative c-subject mb-40">                                   
                                    <input type="text" placeholder="I would like to discuss">
                                </div>
                            </div>							
                            <div class="col-lg-12">
                                <div class="contact-field p-relative c-message mb-45">                                  
                                    <textarea name="message" id="message" cols="30" rows="10" placeholder="Write comments"></textarea>
                                </div>
                                <button class="btn">Send Message</button>
                            </div>
                            </div>
                        
                    </form>
						</div>
					</div>
                    
                </div>
               
            </section>
            <!-- contact-area-end -->
			 <!-- brand-area -->
            <div class="brand-area pt-60 pb-60" style="background-color:#d29751">
                <div class="container">
                    <div class="row brand-active">
                        <div class="col-xl-2">
                            <div class="single-brand">
                                <img src="<?=base_url("assets/img/brand/c-logo1.png")?>"alt="img">
                            </div>
                        </div>
                        <div class="col-xl-2">
                            <div class="single-brand">
                                <img src="<?=base_url("assets/img/brand/c-logo2.png")?>" alt="img">
                            </div>
                        </div>
                        <div class="col-xl-2">
                            <div class="single-brand">
                                <img src="<?=base_url("assets/img/brand/c-logo3.png")?>" alt="img">
                            </div>
                        </div>
                        <div class="col-xl-2">
                            <div class="single-brand">
                                <img src="<?=base_url("assets/img/brand/c-logo4.png")?>" alt="img">
                            </div>
                        </div>
                        <div class="col-xl-2">
                            <div class="single-brand">
                                <img src="<?=base_url("assets/img/brand/c-logo5.png")?>" alt="img">
                            </div>
                        </div>
                        <div class="col-xl-2">
                            <div class="single-brand">
                                <img src="<?=base_url("assets/img/brand/c-logo6.png")?>" alt="img">
                            </div>
                        </div>
                        <div class="col-xl-2">
                            <div class="single-brand">
                                <img src="<?=base_url("assets/img/brand/c-logo1.png")?>" alt="img">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- brand-area-end -->
        </main>
        <!-- main-area-end -->
        