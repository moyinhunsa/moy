<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">

    <!-- ==================
         PAGE CONTENT START
         ================== -->

         <div class="page-content-wrapper">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">The RealPathFinder</a></li>
                                    <li class="breadcrumb-item active">Plans</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Plans</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
                <div class="row">
                    <div class="col-md-12">

                        <div class="card card-body">
                            <p class="text-right">
                                 <button class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> New plan</button>
                            </p>
            <!-- sample modal content -->
                                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title mt-0" id="myModalLabel">Create plans</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>

                                             <form action="<?=site_url("admin/plan_create")?>" method="POST" id="form-plan-create">
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label for="residence"> Residence</label>
                                                            <input type="text" class="form-control" name="residence" id="residence">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="bed_bath">Bed Bath</label>
                                                            <input type="text" class="form-control" name="bed_bath" id="bed_bath">
                                                        </div>
                                                         <div class="form-group col-md-12">
                                                            <label for="sqft">Sqft</label>
                                                            <input type="text" class="form-control" name="sqft" id="sqft">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="sale_price">Sale Price</label>
                                                            <input type="text" class="form-control" name="sale_price" id="sale_price">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="rent_price">Rent Price</label>
                                                            <input type="text" class="form-control" name="rent_price" id="rent_price">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="plan_img">Upload Image</label>
                                                            <input type="file" class="form-control" name="plan_img" id="plan_img">
                                                        </div>
                                                    </div>              
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Create plans</button>
                                                </div>
                                            </form>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->

                             <table class="table table-hover">
                                <thead class="btn-info">
                                <tr>
                                    <th>#</th>
                                    <th>Plan Image</th>
                                    <th>Residence</th>
                                    <th>Bed Bath</th>
                                    <th>SQFT</th>
                                     <th>Sale Price</th>
                                     <th>Rent Price</th>
                                     <th></th>
                                </tr>
                                </thead>
                                 <tbody>
                                <?php
                                $sn = 1;
                                foreach ($plans as $p) {   ?>
                                    
                                    <tr>
                                        <th scope="row"><?=$sn?></th>
                                         <td>
                                            <img class="rounded" width="200" src="<?=planImageSrc($p['plan_img'])?>">
                                        </td>
                                        <td><?= $p['residence']?></td>
                                        <td><?=  $p['bed_bath']?></td>
                                        <td><?=  $p['sqft']?></td>
                                        <td><?=  $p['sale_price']?></td>
                                        <td><?=  $p['rent_price']?></td>
                                         <td>
                                            <a href="<?=site_url("admin/plan_edit?plan_id={$p['plan_id']}") ?>" class="btn btn-warning" ><i class="fa fa-edit"></i></a>
                                            <a href="<?=site_url("admin/plan_delete?plan_id={$p['plan_id']}") ?>" class="btn btn-danger a-plan-delete"><i class="fa fa-times"></i></a>
                                         </td>
                                    </tr>
                               
                                    <?php
                                    $sn ++;
                                } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
               
            </div><!-- container -->

        </div> <!-- Page content Wrapper -->

    </div> <!-- content -->
</div>
 <script type="text/javascript">
   
   $('#form-plan-create').submit(function(e){
        e.preventDefault();

        var xhr = submitForm2('#form-plan-create');
        xhr.done(function(result){
            if(result.status){
               location.reload();
            }
        })
   });

   $('.a-plan-delete').click(function(e){
        e.preventDefault();

        var a = $(this);
        if (confirm('Are you sure you want to delete this plans?')) {

            var xhr = getRequest(a.attr('href'));
            xhr.done(function(result){
                if(result.status){
                    a.parent().parent().fadeOut(4000);
                }
            });
        } 
   })

</script>
<!-- End Right content here -->    
