<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">

    <!-- ==================
         PAGE CONTENT START
         ================== -->

         <div class="page-content-wrapper">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">The RealPathFinder</a></li>
                                    <li class="breadcrumb-item active">User Profile</li>
                                </ol>
                            </div>
                            <h4 class="page-title">User Profile</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
                <div class="row">
                    <div class="offset-md-2  col-md-8">

                        <div class="card card-body">
                            <form action="<?=site_url("admin/update_user")?>" method="POST" id="form-user-edit" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <div class="text-center">
                                            <img class="rounded-circle" id="photo" width="150" src="<?=userPhoto($u['photo'])?>">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="fullname">Full Name</label>
                                        <input type="text" class="form-control" name="fullname" id="fullname" value="<?=$u['fullname']?>">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="email">Email</label>
                                        <input type="text" class="form-control" name="email" id="email" value="<?=$u['email']?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="photo">Upload New Image (Optional)</label>
                                        <input type="file" class="form-control" name="photo" id="photo">
                                    </div>
                                     <div class="form-group col-md-12">
                                        <input type="hidden" name="user_id" value="<?=$u['user_id']?>">
                                       <button class="btn btn-success" type="submit">Update</button>
                                    </div>
                                </div>
                            </form>
                          
                        </div>

                    </div>
                </div>
               
            </div><!-- container -->

        </div> <!-- Page content Wrapper -->

    </div> <!-- content -->
</div>
 <script type="text/javascript">
   
   $('#form-user-edit').submit(function(e){
        e.preventDefault();

        var xhr = submitForm2('#form-user-edit');
        xhr.done(function(result){
            if(result.status){
                if(result.data.photo){
                    $("#photo").attr("src", result.data.photo);
                }
            }
        })
   });

</script>
<!-- End Right content here -->