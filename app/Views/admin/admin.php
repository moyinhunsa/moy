<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">

    <!-- ==================
         PAGE CONTENT START
         ================== -->

         <div class="page-content-wrapper">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">OxfordRealAsset</a></li>
                                    <li class="breadcrumb-item active">Dashboard</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Dashboard</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <div class="row">
                    <div class="col-md-6 col-lg-6 col-xl-3">
                        <div class="mini-stat clearfix bg-white">
                            <span class="mini-stat-icon bg-primary"><i class="mdi mdi-cart-outline"></i></span>
                            <div class="mini-stat-info text-right">
                                <h3 class="counter mt-0 text-primary">$9851</h3>
                            </div>
                            <div class="clearfix"></div>
                            <p class=" mb-0 m-t-20 text-muted">Total Sales: $22506 <span class="pull-right"><i class="fa fa-caret-up text-success m-r-5"></i>10.25%</span></p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-3">
                        <div class="mini-stat clearfix bg-white">
                            <span class="mini-stat-icon bg-success"><i class="mdi mdi-currency-usd"></i></span>
                            <div class="mini-stat-info text-right">
                                <h3 class="counter mt-0 text-success">3514</h3>
                            </div>
                            <div class="clearfix"></div>
                            <p class="text-muted mb-0 m-t-20">Total Orders: 892541 <span class="pull-right"><i class="fa fa-caret-down text-danger m-r-5"></i>8.38%</span></p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-3">
                        <div class="mini-stat clearfix bg-white">
                            <span class="mini-stat-icon bg-primary"><i class="mdi mdi-cube-outline"></i></span>
                            <div class="mini-stat-info text-right">
                                <h3 class="counter mt-0 text-primary">5210</h3>
                            </div>
                            <div class="clearfix"></div>
                            <p class="text-muted mb-0 m-t-20">Total Users: 95,251 <span class="pull-right"><i class="fa fa-caret-up text-success m-r-5"></i>3.05%</span></p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-3">
                        <div class="mini-stat clearfix bg-white">
                            <span class="mini-stat-icon bg-success"><i class="mdi mdi-currency-btc"></i></span>
                            <div class="mini-stat-info text-right">
                                <h3 class="counter mt-0 text-success">32,548</h3>
                            </div>
                            <div class="clearfix"></div>
                            <p class="text-muted mb-0 m-t-20">Average Visitors: 24,511 <span class="pull-right"><i class="fa fa-caret-up text-success m-r-5"></i>22.58%</span></p>
                        </div>
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-xl-7">
                        <div class="card m-b-20">
                            <div class="card-body">

                                <h4 class="mt-0 mb-4 header-title">Monthly Earnings</h4>

                                <ul class="list-inline widget-chart m-t-20 m-b-15 text-center">
                                    <li class="list-inline-item">
                                        <i class="mdi mdi-arrow-up-bold-circle-outline text-success"></i>
                                        <h5 class="mb-0">$1,542</h5>
                                        <p class="text-muted font-14">Weekly Earnings</p>
                                    </li>
                                    <li class="list-inline-item">
                                        <i class="mdi mdi-arrow-down-bold-circle-outline text-danger"></i>
                                        <h5 class="mb-0">$6,451</h5>
                                        <p class="text-muted font-14">Monthly Earnings</p>
                                    </li>
                                    <li class="list-inline-item">
                                        <i class="mdi mdi-arrow-up-bold-circle-outline text-success"></i>
                                        <h5 class="mb-0">$84,574</h5>
                                        <p class="text-muted font-14">Yearly Earnings</p>
                                    </li>
                                </ul>

                                <div id="stacked-bar-chart" class="ct-chart ct-golden-section" style="height: 360px;"></div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                    <div class="col-xl-5">
                        <div class="card m-b-20">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Sales Analytics</h4>

                                <ul class="list-inline widget-chart m-t-20 m-b-15 text-center">
                                    <li class="list-inline-item">
                                        <i class="mdi mdi-arrow-up-bold-circle-outline text-success"></i>
                                        <h5 class="mb-0">$1,542</h5>
                                        <p class="text-muted font-14">Weekly Earnings</p>
                                    </li>
                                    <li class="list-inline-item">
                                        <i class="mdi mdi-arrow-down-bold-circle-outline text-danger"></i>
                                        <h5 class="mb-0">$6,451</h5>
                                        <p class="text-muted font-14">Monthly Earnings</p>
                                    </li>
                                </ul>

                                <div id="simple-pie" class="ct-chart ct-golden-section simple-pie-chart-chartist"></div>

                                <p class="mt-3 mb-0 text-center text-muted">
                                    Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites..
                                </p>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

                <div class="row">
                    <div class="col-xl-3">
                        <div class="card m-b-20">
                            <div class="card-body">
                                <h4 class="mt-0 header-title m-b-30">Recent Stock</h4>

                                <div class="text-center">
                                    <input class="knob" data-width="120" data-height="120" data-linecap=round
                                    data-fgColor="#e8474c" value="80" data-skin="tron" data-angleOffset="180"
                                    data-readOnly=true data-thickness=".1"/>

                                    <div class="clearfix"></div>
                                    <a href="#" class="btn btn-sm btn-danger m-t-20">View All Data</a>
                                    <ul class="list-inline row m-t-30 clearfix">
                                        <li class="col-6">
                                            <p class="m-b-5 font-18 font-500">7,541</p>
                                            <p class="mb-0">Mobile Phones</p>
                                        </li>
                                        <li class="col-6">
                                            <p class="m-b-5 font-18 font-500">125</p>
                                            <p class="mb-0">Desktops</p>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3">
                        <div class="card m-b-20">
                            <div class="card-body">
                                <h4 class="mt-0 header-title m-b-30">Purchase Order</h4>

                                <div class="text-center">
                                    <input class="knob" data-width="120" data-height="120" data-linecap=round
                                    data-fgColor="#4ac18e" value="68" data-skin="tron" data-angleOffset="180"
                                    data-readOnly=true data-thickness=".1"/>

                                    <div class="clearfix"></div>
                                    <a href="#" class="btn btn-sm btn-success m-t-20">View All Data</a>
                                    <ul class="list-inline row m-t-30 clearfix">
                                        <li class="col-6">
                                            <p class="m-b-5 font-18 font-500">2,541</p>
                                            <p class="mb-0">Mobile Phones</p>
                                        </li>
                                        <li class="col-6">
                                            <p class="m-b-5 font-18 font-500">874</p>
                                            <p class="mb-0">Desktops</p>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3">
                        <div class="card m-b-20">
                            <div class="card-body">
                                <h4 class="mt-0 header-title m-b-30">Shipped Orders</h4>

                                <div class="text-center">
                                    <input class="knob" data-width="120" data-height="120" data-linecap=round
                                    data-fgColor="#8d6e63" value="39" data-skin="tron" data-angleOffset="180"
                                    data-readOnly=true data-thickness=".1"/>

                                    <div class="clearfix"></div>
                                    <a href="#" class="btn btn-sm btn-brown m-t-20">View All Data</a>
                                    <ul class="list-inline row m-t-30 clearfix">
                                        <li class="col-6">
                                            <p class="m-b-5 font-18 font-500">1,154</p>
                                            <p class="mb-0">Mobile Phones</p>
                                        </li>
                                        <li class="col-6">
                                            <p class="m-b-5 font-18 font-500">89</p>
                                            <p class="mb-0">Desktops</p>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3">
                        <div class="card m-b-20">
                            <div class="card-body">
                                <h4 class="mt-0 header-title m-b-30">Cancelled Orders</h4>

                                <div class="text-center">
                                    <input class="knob" data-width="120" data-height="120" data-linecap=round
                                    data-fgColor="#90a4ae" value="95" data-skin="tron" data-angleOffset="180"
                                    data-readOnly=true data-thickness=".1"/>

                                    <div class="clearfix"></div>
                                    <a href="#" class="btn btn-sm btn-blue-grey m-t-20">View All Data</a>
                                    <ul class="list-inline row m-t-30 clearfix">
                                        <li class="col-6">
                                            <p class="m-b-5 font-18 font-500">95</p>
                                            <p class="mb-0">Mobile Phones</p>
                                        </li>
                                        <li class="col-6">
                                            <p class="m-b-5 font-18 font-500">25</p>
                                            <p class="mb-0">Desktops</p>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- end row -->


            </div><!-- container -->

        </div> <!-- Page content Wrapper -->

    </div> <!-- content -->
</div>

<!-- End Right content here -->
   
