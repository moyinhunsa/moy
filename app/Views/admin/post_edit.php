<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">

    <!-- ==================
         PAGE CONTENT START
         ================== -->

         <div class="page-content-wrapper">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">OxfordRealAssets</a></li>
                                    <li class="breadcrumb-item active">Edit Post</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Edit Post</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
                <div class="row">
                    <div class="offset-md-3 col-md-6">

                        <div class="card card-body">
                            <form action="<?=site_url("admin/post_edit_process")?>" method="POST" id="form-post-edit" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <img class="rounded img-fluid" id="post_img" src="<?=postImageSrc($p['post_img'])?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="post_title">Post Title</label>
                                        <input type="text" class="form-control" name="post_title" id="post_title" value="<?=$p['post_title']?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="post_details">Posts Details</label>
                                        <input type="text" class="form-control" name="post_details" id="post_details" value="<?=$p['post_details']?>">
                                    </div>
                                     <div class="form-group col-md-12">
                                        <label for="fullname">Posts User</label>
                                        <input type="text" class="form-control" name="fullname" id="fullname" value="<?=$p['fullname']?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="post_img">Upload New Image (Optional)</label>
                                        <input type="file" class="form-control" name="post_img" id="post_img">
                                    </div>
                                     <div class="form-group col-md-12">
                                        <input type="hidden" name="post_id" value="<?=$p['post_id']?>">
                                       <button class="btn btn-success" type="submit">Update</button>
                                    </div>
                                </div>
                            </form>
                          
                        </div>

                    </div>
                </div>
               
            </div><!-- container -->

        </div> <!-- Page content Wrapper -->

    </div> <!-- content -->
</div>
 <script type="text/javascript">
   
   $('#form-post-edit').submit(function(e){
         e.preventDefault();

     var xhr = submitForm2('#form-post-edit');
     xhr.done(function(result){
            if(result.status){
                if(result.data.post_img){
                    $("#post_img").attr("src", result.data.post_img);
                }
            }
        })
   });

</script>
<!-- End Right content here -->
   
