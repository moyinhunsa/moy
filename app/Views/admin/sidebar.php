
            <div class="left side-menu">

                <div class="sidebar-inner slimscrollleft">
                    <div id="sidebar-menu">
                        <ul>

                            <li>
                                <a href="<?=site_url("admin/admin")?>" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span> Dashboard <span class="badge badge-pill badge-success pull-right">02</span> </span></a>
                            </li>

                            <li>
                                <a href="<?=site_url("admin/banner")?>" class="waves-effect"><i class="mdi mdi-email-outline"></i><span> Banners </span></a>
                            </li>

                            <li>
                                <a href="<?=site_url("admin/about")?>" class="waves-effect"><i class="mdi mdi-calendar-check"></i><span> About Us </span></a>
                            </li>

                            <li>
                                <a href="<?=site_url("admin/services")?>" class="waves-effect"><i class="mdi mdi-buffer"></i> <span> Services</span> </a>
                            </li>
                                
                            <li>
                                <a href="<?=site_url("admin/testimonials")?>" class="waves-effect"><i class="mdi mdi-clipboard-outline"></i><span> Testimonials </span></a>
                            </li>

                            <li>
                                <a href="<?=site_url("admin/sitesetting")?>" class="waves-effect"><i class="mdi mdi-chart-line"></i><span>Site Setting</span></a>
                            </li>

                            <li>
                                <a href="<?=site_url("admin/posts")?>" class="waves-effect"><i class="mdi mdi-format-list-bulleted-type"></i><span> Posts </span></a>
                            </li>

                            <li>
                                <a href="<?=site_url("admin/plans")?>" class="waves-effect"><i class="mdi mdi-album"></i> <span> Plans  </span> </a>
                            </li>

                            <li>
                                <a href="<?=site_url("admin/user")?>" class="waves-effect"><i class="fa fa-user"></i><span> Users </span></a>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div> <!-- end sidebarinner -->
            </div>
            <!-- Left Sidebar End -->
