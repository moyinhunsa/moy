<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">

    <!-- ==================
         PAGE CONTENT START
         ================== -->

         <div class="page-content-wrapper">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">OxfordRealEstate</a></li>
                                    <li class="breadcrumb-item active">Edit plan</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Edit plan</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
                <div class="row">
                    <div class="offset-md-3 col-md-6">

                        <div class="card card-body">
                            <form action="<?=site_url("admin/plan_edit_process")?>" method="POST" id="form-plan-edit" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <img class="rounded img-fluid" id="plan_img" src="../<?=planImageSrc($p['plan_img'])?>">
                                    </div>
                                   <div class="form-group col-md-12">
                                        <label for="residence"> Residence</label>
                                        <input type="text" class="form-control" name="residence" id="residence" value="<?=$p['residence']?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="bed_bath">Bed Bath</label>
                                        <input type="text" class="form-control" name="bed_bath" id="bed_bath" value="<?=$p['bed_bath']?>">
                                    </div>
                                     <div class="form-group col-md-12">
                                        <label for="sqft">Square feet</label>
                                        <input type="text" class="form-control" name="sqft" id="sqft" value="<?=$p['sqft']?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="sale_price">Sale Price</label>
                                        <input type="text" class="form-control" name="sale_price" id="sale_price" value="<?=$p['sale_price']?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="rent_price">Rent Price</label>
                                        <input type="text" class="form-control" name="rent_price" id="rent_price" value="<?=$p['rent_price']?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="plan_img">Upload New Image (Optional)</label>
                                        <input type="file" class="form-control" name="plan_img" id="plan_img">
                                    </div>
                                     <div class="form-group col-md-12">
                                        <input type="hidden" name="plan_id" value="<?=$p['plan_id']?>">
                                       <button class="btn btn-success" type="submit">Update</button>
                                    </div>
                                </div>
                            </form>
                          
                        </div>

                    </div>
                </div>
               
            </div><!-- container -->

        </div> <!-- Page content Wrapper -->

    </div> <!-- content -->
</div>
 <script type="text/javascript">
   
   $('#form-plan-edit').submit(function(e){
         e.preventDefault();

     var xhr = submitForm2('#form-plan-edit');
     xhr.done(function(result){
            if(result.status){
                if(result.data.plan_img){
                    $("#plan_img").attr("src", result.data.plan_img);
                }
            }
        })
   });

</script>
<!-- End Right content here -->
<?php
include "footer.php";
?>     
