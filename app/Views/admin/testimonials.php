
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">

    <!-- ==================
         PAGE CONTENT START
         ================== -->

         <div class="page-content-wrapper">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">OxfordRealEstateSolutions</a></li>
                                    <li class="breadcrumb-item active">Testimonial</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Testimonial</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
                <div class="row">
                    <div class="col-md-12">

                        <div class="card card-body">
                            <p class="text-right">
                                 <button class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> New Testimonial</button>
                            </p>
                                 <!-- sample modal content -->
                                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title mt-0" id="myModalLabel">Create Testimonial</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>

                                             <form action="<?=site_url("admin/testimonial_create")?>" method="POST" id="form-testimonial-create">
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label for="client_img">Client Image</label>
                                                            <input type="file" class="form-control" name="client_img" id="client_img">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="testimony">Testimony</label>
                                                            <textarea class="form-control" name="testimony" id="testimony"></textarea>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="client_name">client Name</label>
                                                            <input type="text" class="form-control" name="client_name" id="client_name">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="client_desc">Client Description</label>
                                                            <input type="text" class="form-control" name="client_desc" id="client_desc">
                                                        </div>
                                                    </div>              
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Create Testimonial</button>
                                                </div>
                                            </form>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->

                             <table class="table table-hover">
                                <thead class="btn-info">
                                <tr>
                                    <th>#</th>
                                    <th>Client Image</th>
                                    <th>Testimony</th>
                                    <th>Client Name</th>
                                    <th>Client Description</th>
                                    <th></th>
                                     <th></th>
                                </tr>
                                </thead>
                                 <tbody>
                                <?php
                                $sn = 1;
                                foreach ($testimony as $t) {   ?>
                                    
                                    <tr>
                                        <th scope="row"><?=$sn?></th>
                                        <td><img class="rounded" width="50" src="<?=clientImageSrc($t['client_img'])?>"></td>
                                        <td><?= reduce_text($t['testimony'])?>...</td>
                                        <td><?=  $t['client_name']?></td>
                                        <td><?=  $t['client_desc']?></td>
                                        <td>
                                            <a href="<?=site_url("admin/testimonial_edit?id={$t['id']}")?>" class="btn btn-warning" ><i class="fa fa-edit"></i></a>
                                        </td>
                                        <td>
                                            <a href="<?=site_url("admin/testimonial_delete?id={$t['id']}")?>" class="btn btn-danger a-testimonial-delete"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                               
                                    <?php
                                    $sn ++;
                                } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
               
            </div><!-- container -->

        </div> <!-- Page content Wrapper -->

    </div> <!-- content -->
</div>
 <script type="text/javascript">
   
   $('#form-testimonial-create').submit(function(e){
        e.preventDefault();

        var xhr = submitForm2('#form-testimonial-create');
        xhr.done(function(result){
            if(result.status){
               location.reload();
            }
        })
   });

   $('.a-testimonial-delete').click(function(e){
        e.preventDefault();

        var a = $(this);
        if (confirm('Are you sure you want to delete this testimonial?')) {

            var xhr = getRequest(a.attr('href'));
            xhr.done(function(result){
                if(result.status){
                    a.parent().parent().fadeOut(4000);
                }
            });
        } 
   })

</script>
<!-- End Right content here -->