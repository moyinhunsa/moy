<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">

    <!-- ==================
         PAGE CONTENT START
         ================== -->

         <div class="page-content-wrapper">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb p-0 m-0">
                                    <li class="breadcrumb-item"><a href="index">OxfordRealAsset</a></li>
                                    <li class="breadcrumb-item active">Banners</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Banners</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
                <div class="row">
                    <div class="col-md-12">

                        <div class="card card-body">
                            <p class="text-right">
                                 <button class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> New banner</button>
                            </p>
            <!-- sample modal content -->
                                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title mt-0" id="myModalLabel">Create Banner</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                             <form action="<?=site_url("admin/banner_create")?>" method="POST" id="form-banner-create" enctype="multipart/form-data">
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label for="banner_title">Banner Title</label>
                                                            <input type="text" class="form-control" name="banner_title" id="banner_title">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="banner_price">Banner Price</label>
                                                            <input type="text" class="form-control" name="banner_price" id="banner_price">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="banner_img">Upload Image</label>
                                                            <input type="file" class="form-control" name="banner_img" id="banner_img">
                                                        </div>
                                                    </div>              
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Create Banner</button>
                                                </div>
                                            </form>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->

                             <table class="table table-hover">
                                <thead class="btn-info">
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Price</th>
                                     <th></th>
                                </tr>
                                </thead>
                                 <tbody>
                                <?php
                                $sn = 1;
                                foreach ($banners as $b) {   ?>
                                    
                                    <tr>
                                        <th scope="row"><?= $sn?></th>
                                        <td>
                                            <img class="rounded" width="200" src="<?=bannerImageSrc($b['banner_img'])?>">
                                        </td>
                                        <td><?= $b['banner_title']?></td>
                                        <td><?=  $b['banner_price']?></td>
                                         <td>
                                            <a href="<?=site_url("admin/banner_edit?banner_id={$b['banner_id']}")?>" class="btn btn-warning" ><i class="fa fa-edit"></i></a>
                                            <a href="<?=site_url("admin/banner_delete?banner_id={$b['banner_id']}")?>" class="btn btn-danger a-banner-delete"><i class="fa fa-times"></i></a>
                                         </td>
                                    </tr>
                               
                                    <?php
                                    $sn ++;
                                } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
               
            </div><!-- container -->

        </div> <!-- Page content Wrapper -->

    </div> <!-- content -->
</div>
 <script type="text/javascript">
   
   $('#form-banner-create').submit(function(e){
        e.preventDefault();

        var xhr = submitForm2('#form-banner-create');
        xhr.done(function(result){
            if(result.status){
               location.reload();
            }
        })
   });

   $('.a-banner-delete').click(function(e){
        e.preventDefault();

        var a = $(this);
        if (confirm('Are you sure you want to delete this Banner?')) {

            var xhr = getRequest(a.attr('href'));
            xhr.done(function(result){
                if(result.status){
                    a.parent().parent().fadeOut(4000);
                }
            });
        } 
   })

</script>
<!-- End Right content here -->    
