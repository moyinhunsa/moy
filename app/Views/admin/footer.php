</div>
        <!-- END wrapper -->

	<footer class="footer">
	© 2020 OxfordRealAssetSolution <span class="text-muted hide-sm pull-right">Crafted with <i class="mdi mdi-heart text-danger"></i> by Mo</span>
	</footer>

	<!-- jQuery  -->

	<script src="<?=base_url("assets/admin/js/popper.min.js")?>"></script>
	<script src="<?=base_url("assets/admin/js/bootstrap.min.js")?>"></script>
	<script src="<?=base_url("assets/admin/js/modernizr.min.js")?>"></script>
	<script src="<?=base_url("assets/admin/js/jquery.slimscroll.js")?>"></script>
	<script src="<?=base_url("assets/admin/js/waves.js")?>"></script>
	<script src="<?=base_url("assets/admin/js/jquery.nicescroll.js")?>"></script>
	<script src="<?=base_url("assets/admin/js/jquery.scrollTo.min.js")?>"></script>

	<!--Chartist Chart-->
	<script src="<?=base_url("assets/admin/plugins/chartist/js/chartist.min.js")?>"></script>
	<script src="<?=base_url("assets/admin/plugins/chartist/js/chartist-plugin-tooltip.min.js")?>"></script>

	<!-- KNOB JS -->
	<script src="<?=base_url("assets/admin/plugins/jquery-knob/excanvas.js")?>"></script>
	<script src="<?=base_url("assets/admin/plugins/jquery-knob/jquery.knob.js")?>"></script>

	<!-- Dashboard init -->
	<script src="<?=base_url("assets/admin/pages/dashboard.js")?>"></script>

	<!-- App js -->
	<script src="<?=base_url("assets/admin/js/app.js")?>"></script>
	<script src="<?=base_url("assets/admin/js/bootstrap-notify.js")?>"></script>
    <script src="<?=base_url("assets/admin/js/mo.js")?>"></script>
    <script src="<?=base_url("assets/admin/js/sweetalert.min.js")?>"></script>

	</body>
	</html>