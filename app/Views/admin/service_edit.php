<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">

    <!-- ==================
         PAGE CONTENT START
         ================== -->

         <div class="page-content-wrapper">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">The RealPathFinder</a></li>
                                    <li class="breadcrumb-item active">Edit Service</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Edit Service</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
                <div class="row">
                    <div class="offset-md-3 col-md-6">

                        <div class="card card-body">
                            <form action="<?=site_url("admin/service_edit_process")?>" method="POST" id="form-service-edit">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="service_title">Service Title</label>
                                        <input type="text" class="form-control" name="service_title" id="service_title" value="<?=$s['service_title']?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="service_icon">Service Icon</label>
                                        <input type="text" class="form-control" name="service_icon" id="service_icon" value="<?=$s['service_icon']?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="service_desc">Service Description</label>
                                        <textarea class="form-control" name="service_desc" id="service_desc"><?=$s['service_desc']?></textarea>
                                    </div>
                                     <div class="form-group col-md-12">
                                        <input type="hidden" name="service_id" value="<?=$s['service_id']?>">
                                       <button class="btn btn-success" type="submit">Update</button>
                                    </div>
                                </div>
                            </form>
                          
                        </div>

                    </div>
                </div>
               
            </div><!-- container -->

        </div> <!-- Page content Wrapper -->

    </div> <!-- content -->
</div>
 <script type="text/javascript">
   
   $('#form-service-edit').submit(function(e){
        e.preventDefault();

        submitForm2('#form-service-edit');
   });

</script>
<!-- End Right content here -->
