<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">

    <!-- ==================
         PAGE CONTENT START
         ================== -->

         <div class="page-content-wrapper">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">OxfordRealAsset</a></li>
                                    <li class="breadcrumb-item active">Edit Banner</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Edit Banner</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
                <div class="row">
                    <div class="offset-md-3 col-md-6">

                        <div class="card card-body">
                            <form action="<?=site_url("admin/banner_edit_process")?>" method="POST" id="form-banner-edit" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <img class="rounded img-fluid" id="banner_img" src="<?=bannerImageSrc($b['banner_img'])?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="banner_title">Banner Title</label>
                                        <input type="text" class="form-control" name="banner_title" id="banner_title" value="<?=$b['banner_title']?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="banner_price">Banner Price</label>
                                        <input type="text" class="form-control" name="banner_price" id="banner_price" value="<?=$b['banner_price']?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="banner_img">Upload New Image (Optional)</label>
                                        <input type="file" class="form-control" name="banner_img" id="banner_img">
                                    </div>
                                     <div class="form-group col-md-12">
                                        <input type="hidden" name="banner_id" value="<?=$b['banner_id']?>">
                                       <button class="btn btn-success" type="submit">Update</button>
                                    </div>
                                </div>
                            </form>
                          
                        </div>

                    </div>
                </div>
               
            </div><!-- container -->

        </div> <!-- Page content Wrapper -->

    </div> <!-- content -->
</div>
 <script type="text/javascript">
   
   $('#form-banner-edit').submit(function(e){
         e.preventDefault();

     var xhr = submitForm2('#form-banner-edit');
     xhr.done(function(result){
            if(result.status){
                if(result.data.banner_img){
                    $("#banner_img").attr("src", result.data.banner_img);
                }
            }
        })
   });

</script>
<!-- End Right content here -->