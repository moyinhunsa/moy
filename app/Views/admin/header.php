<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Admin Dashboard</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Themesbrand" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App Icons -->
        <link rel="shortcut icon" href="<?=base_url("assets/admin/images/favicon.ico")?>">

        <!--Chartist Chart CSS -->
        <link rel="stylesheet" href="<?=base_url("assets/admin/plugins/chartist/css/chartist.min.css")?>">

        <!-- Basic Css files -->
        <link href="<?=base_url("assets/admin/css/bootstrap.min.css")?>" rel="stylesheet" type="text/css">
        <link href="<?=base_url("assets/admin/css/icons.css")?>" rel="stylesheet" type="text/css">
        <link href="<?=base_url("assets/admin/css/style.css?v=1")?>" rel="stylesheet" type="text/css">
        <link href="<?=base_url("assets/admin/css/animate.css")?>" rel="stylesheet" type="text/css">

        <script src="<?=base_url("assets/admin/js/jquery.min.js")?>"></script>
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader" style="opacity: 0.4">
            <div id="status">
                <div class="sk-three-bounce">
                    <div class="sk-child sk-bounce1"></div>
                    <div class="sk-child sk-bounce2"></div>
                    <div class="sk-child sk-bounce3"></div>
                </div>
            </div>
        </div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">
                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="">
                        <a href="index" class="logo text-center">OxfordRealAsset</a>
                        <!--<a href="admin" class="logo"><img src="<?=base_url("assets/admin/images/logo.png")?>" height="14" alt="logo"></a>-->
                    </div>
                </div>

                <nav class="navbar-custom">
                    <!-- Search input -->
                    <div class="search-wrap" id="search-wrap">
                        <div class="search-bar">
                            <input class="search-input" type="search" placeholder="Search" />
                            <a href="#" class="close-search toggle-search" data-target="#search-wrap">
                                <i class="mdi mdi-close-circle"></i>
                            </a>
                        </div>
                    </div>

                    <ul class="list-inline float-right mb-0">
                        <!-- Search -->
                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link waves-effect toggle-search" href="#"  data-target="#search-wrap">
                                <i class="mdi mdi-magnify noti-icon"></i>
                            </a>
                        </li>
                        <!-- Fullscreen -->
                        <li class="list-inline-item dropdown notification-list hide-sm">
                            <a class="nav-link waves-effect" href="#" id="btn-fullscreen">
                                <i class="mdi mdi-fullscreen noti-icon"></i>
                            </a>
                        </li>
                        <!-- language-->
                        <li class="list-inline-item dropdown notification-list hide-sm">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect text-muted" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                English <img src="<?=base_url("assets/admin/images/flags/us_flag.jpg")?>" class="ml-2" height="16" alt=""/>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right language-switch">
                                <a class="dropdown-item" href="#"><img src="<?=base_url("assets/admin/images/flags/germany_flag.jpg")?>" alt="" height="16"/><span> German </span></a>
                                <a class="dropdown-item" href="#"><img src="<?=base_url("assets/admin/images/flags/italy_flag.jpg")?>" alt="" height="16"/><span> Italian </span></a>
                                <a class="dropdown-item" href="#"><img src="<?=base_url("assets/admin/images/flags/french_flag.jpg")?>" alt="" height="16"/><span> French </span></a>
                                <a class="dropdown-item" href="#"><img src="<?=base_url("assets/admin/images/flags/spain_flag.jpg")?>" alt="" height="16"/><span> Spanish </span></a>
                                <a class="dropdown-item" href="#"><img src="<?=base_url("assets/admin/images/flags/russia_flag.jpg")?>" alt="" height="16"/><span> Russian </span></a>
                                <a class="dropdown-item" href="#"><img src="<?=base_url("assets/admin/images/flags/russia_flag.jpg")?>" alt="" height="16"/><span> Nigerian </span></a>
                            </div>
                        </li>
                        <!-- notification-->
                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <i class="ion-ios7-bell noti-icon"></i>
                                <span class="badge badge-danger noti-icon-badge"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5>Notification (5)</h5>
                                </div>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                    <div class="notify-icon bg-primary"><i class="mdi mdi-cart-outline"></i></div>
                                    <p class="notify-details"><b>Your order is placed</b><small class="text-muted">Dummy text of the printing and typesetting industry.</small></p>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-success"><i class="mdi mdi-message"></i></div>
                                    <p class="notify-details"><b>New Message received</b><small class="text-muted">You have 87 unread messages</small></p>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-info"><i class="mdi mdi-martini"></i></div>
                                    <p class="notify-details"><b>Your item is shipped</b><small class="text-muted">It is a long established fact that a reader will</small></p>
                                </a>

                                <!-- All-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    View All
                                </a>

                            </div>
                        </li>
                        <!-- User-->
                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                              <span style="color: white; text-transform: capitalize;"> <?= user('fullname')?></span>
                                <img src="<?=userPhoto(user('photo'))?>" alt="user" class="rounded-circle">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <a class="dropdown-item" href="<?=site_url("admin/user_profile")?>"><i class="dripicons-user text-muted"></i> Profile</a>
                                <a class="dropdown-item" href="<?=site_url("admin/change_password?user_id= {['user_id']}")?>"><i class="dripicons-wallet text-muted"></i>Change Password</a>
                                <a class="dropdown-item" href="#"><span class="badge badge-success pull-right m-t-5">5</span><i class="dripicons-gear text-muted"></i> Settings</a>
                                <a class="dropdown-item" href="#"><i class="dripicons-lock text-muted"></i> Lock screen</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?= site_url('login/logout')?>"><i class="dripicons-exit text-muted"></i> Logout</a>
                            </div>
                        </li>
                    </ul>

                    <!-- Menu Collapse Button -->
                    <button type="button" class="button-menu-mobile open-left waves-effect">
                       <i class="fa fa-reorder" style="font-size:36px;color:white"></i>
                    </button>

                    <div class="clearfix"></div>
                </nav>

            </div>
            <!-- Top Bar End -->