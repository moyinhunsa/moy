<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">

    <!-- ==================
         PAGE CONTENT START
         ================== -->

         <div class="page-content-wrapper">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">OxfordRealAsset</a></li>
                                    <li class="breadcrumb-item active">Change Password</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Change Password</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
                <div class="row">
                    <div class="offset-md-2  col-md-8">

                        <div class="card card-body">
                            <form action="<?=site_url("admin/update_password")?>" method="POST" id="form-password-edit" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="password">Current Password</label>
                                        <input type="text" class="form-control" name="password" id="password">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="new_password">New Password</label>
                                        <input type="text" class="form-control" name="new_password" id="new_password">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="confirm_password">Confirm New Password</label>
                                        <input type="text" class="form-control" name="confirm_password" id="confirm_password">
                                    </div>
                                    <div class="text-center">
                                     <div class="form-group col-md-12">
                                       <button class="btn btn-success" type="submit">Update</button>
                                    </div>
                                    </div>
                                </div>
                            </form>
                          
                        </div>

                    </div>
                </div>
               
            </div><!-- container -->

        </div> <!-- Page content Wrapper -->

    </div> <!-- content -->
</div>
 <script type="text/javascript">
   
   $('#form-password-edit').submit(function(e){
        e.preventDefault();

        var xhr = submitForm2('#form-password-edit');
        xhr.done(function(result){
            (result.status)
          
        })
   });

</script>
<!-- End Right content here -->
