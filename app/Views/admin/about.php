<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">

    <!-- ==================
         PAGE CONTENT START
         ================== -->

         <div class="page-content-wrapper">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">OxfordRealAsset</a></li>
                                    <li class="breadcrumb-item active">Edit About Us</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Edit About Us</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
                <div class="row">
                    <div class="offset-md-3 col-md-6">

                        <div class="card card-body">
                            <form action="about_edit_process" method="POST" id="form-about-edit" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <img class="rounded img-fluid" id="about_img" src="<?=aboutUsImageSrc($about_us['about_img'])?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="title">About Title</label>
                                        <input type="text" class="form-control" name="title" id="title" value="<?=$about_us['title']?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                    <label for="short_desc">Short Description</label>
                                      <textarea class="form-control" name="short_desc"><?=$about_us['short_desc']?></textarea>
                                        </div>
                                      <div class="form-group col-md-12">
                                      <label for="long_desc">Long Description</label>
                                     <textarea class="form-control" name="long_desc" rows="5"><?=$about_us['long_desc']?></textarea>
                                   </div>
                                      <div class="form-group col-md-12"> 
                                         <label for="experience">Experience</label>
                                         <input type="number" name="experience" class="form-control" min="1" max="1000" value="<?=$about_us['experience']?>">
                                      </div>
                                    <div class="form-group col-md-12">
                                        <label for="about_img">Upload New Image (Optional)</label>
                                        <input type="file" class="form-control" name="about_img" id="about_img">
                                    </div>
                                     <div class="form-group col-md-12">
                                       <button class="btn btn-success" type="submit">Update</button>
                                    </div>
                                </div>
                              
                            </form>
                          
                        </div>

                    </div>
                </div>
               
            </div><!-- container -->

        </div> <!-- Page content Wrapper -->

    </div> <!-- content -->
</div>
 <script type="text/javascript">
   
   $('#form-about-edit').submit(function(e){
         e.preventDefault();

     var xhr = submitForm2('#form-about-edit');
     xhr.done(function(result){
            if(result.status){
                if(result.data.about_img){
                    $("#about_img").attr("src", result.data.about_img);
                }
            }
        })
   });

</script>
<!-- End Right content here -->
   
