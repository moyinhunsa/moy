
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">

    <!-- ==================
         PAGE CONTENT START
         ================== -->

         <div class="page-content-wrapper">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">OxfordRealEstate</a></li>
                                    <li class="breadcrumb-item active">Edit Testimonial</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Edit Testimonial</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
                <div class="row">
                    <div class="offset-md-3 col-md-6">

                        <div class="card card-body">
                            <form action="<?=site_url("admin/testimonial_edit_process")?>" method="POST" id="form-testimonial-edit" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <img class="rounded img-fluid" id="client_img" src="../<?=clientImageSrc($t['client_img'])?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="testimony">Testimonial</label>
                                        <input type="text" class="form-control" name="testimony" id="testimony" value="<?=$t['testimony']?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="client_name">Client Name</label>
                                        <input type="text" class="form-control" name="client_name" id="client_name" value="<?=$t['client_name']?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="client_desc">Client Description</label>
                                        <input type="text" class="form-control" name="client_desc" id="client_desc" value="<?=$t['client_desc']?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="client_img">Upload New Image (Optional)</label>
                                        <input type="file" class="form-control" name="client_img" id="client_img">
                                    </div>
                                     <div class="form-group col-md-12">
                                        <input type="hidden" name="id" value="<?=$t['id']?>">
                                       <button class="btn btn-success" type="submit">Update</button>
                                    </div>
                                </div>
                            </form>
                          
                        </div>

                    </div>
                </div>
               
            </div><!-- container -->

        </div> <!-- Page content Wrapper -->

    </div> <!-- content -->
</div>
 <script type="text/javascript">
   
   $('#form-testimonial-edit').submit(function(e){
        e.preventDefault();

        var xhr = submitForm2('#form-testimonial-edit');
        xhr.done(function(result){
            if(result.status){
                if(result.data.client_img){
                    $("#client_img").attr("src", result.data.client_img);
                }
            }
        })
   });

</script>
<!-- End Right content here -->