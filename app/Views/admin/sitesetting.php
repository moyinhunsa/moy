
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">

    <!-- ==================
         PAGE CONTENT START
         ================== -->

         <div class="page-content-wrapper">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">The RealPathFinder</a></li>
                                    <li class="breadcrumb-item active">Edit Site Information</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Edit Site Information</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
                <div class="row">
                    <div class="offset-md-3 col-md-6">

                        <div class="card card-body">
                            <form action="<?=site_url("admin/site_edit_process")?>" method="POST" id="form-site-edit">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <img class="rounded img-fluid" id="site_logo" name="site_logo" src="<?=siteImageSrc($s['site_logo'])?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="site_email">Site Email</label>
                                        <input type="text" class="form-control" name="site_email" id="site_email" value="<?=$s['site_email']?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="site_phone">Site Phone Number</label>
                                        <input type="text" class="form-control" name="site_phone" id="site_phone" value="<?=$s['site_phone']?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="site_address">Site Address</label>
                                        <input type="text" class="form-control" name="site_address" id="site_address" value="<?=$s['site_address']?>">
                                    </div>
                                      <div class="form-group col-md-12">
                                        <label for="site_working_hours">Site Working Hours</label>
                                        <input type="text" class="form-control" name="site_working_hours" id="site_working_hours" value="<?=$s['site_working_hours']?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="footer_line">Footer Line</label>
                                        <textarea class="form-control" name="footer_line" id="footer_line"><?=$s['footer_line']?></textarea>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="site_logo">Upload new Logo (Optional)</label>
                                          <input type="file" class="form-control" name="site_logo" id="site_logo">
                                    </div>
                                     <div class="form-group col-md-12">
                                       <button class="btn btn-success" type="submit">Save Changes</button>
                                    </div>
                                </div>
                            </form>
                          
                        </div>

                    </div>
                </div>
               
            </div><!-- container -->

        </div> <!-- Page content Wrapper -->

    </div> <!-- content -->
</div>
 <script type="text/javascript">
   
   $('#form-site-edit').submit(function(e){
         e.preventDefault();

     var xhr = submitForm2('#form-site-edit');
     xhr.done(function(result){
          (result.status.data)
        })
   });

</script>
<!-- End Right content here -->