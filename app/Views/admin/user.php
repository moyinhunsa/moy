<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">

    <!-- ==================
         PAGE CONTENT START
         ================== -->

         <div class="page-content-wrapper">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">OxfordRealEstate</a></li>
                                    <li class="breadcrumb-item active">Users</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Users</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
                <div class="row">
                    <div class="col-md-12">

                        <div class="card card-body">
                            <p class="text-right">
                                 <button class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> New User</button>
                            </p>
            <!-- sample modal content -->
                                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title mt-0" id="myModalLabel">Create User</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>

                                             <form action="<?=site_url("admin/user_create")?>" method="POST" id="form-user-create">
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label for="fullname">Fullname </label>
                                                            <input type ="text" class="form-control" rows="5" name="fullname" id="fullname">
                                                        </div>
                                                         <div class="form-group col-md-12">
                                                            <label for="email">Email</label>
                                                            <input type="text" class="form-control" name="email" id="email">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="password">Password</label>
                                                            <input type="password" class="form-control" name="password" id="password">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="photo">Upload Image</label>
                                                            <input type="file" class="form-control" name="photo" id="photo">
                                                        </div>
                                                    </div>              
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Create user</button>
                                                </div>
                                            </form>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->

                             <table class="table table-hover">
                                <thead class="btn-info">
                                <tr>
                                    <th>#</th>
                                    <th>Photo</th>
                                    <th>Email</th>
                                    <th>Fullname</th>
                                    <th>Manage Account</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $sn = 1;
                                foreach ($users as $u) {   ?>
                                    
                                    <tr>
                                        <th scope="row"><?= $sn ?></th>
                                        <td>
                                            <img class="rounded-circle" width="50" src="<?=userPhoto($u['photo'])?>">
                                        </td>
                                        <td><?=  $u['email']?></td>
                                        <td style="text-transform:capitalize;"><?=  $u['fullname']?></td>
                                        <td>
                                            <a href="<?=site_url("admin/user_profile?user_id={$u['user_id']}")?>" class="btn btn-success" ><i class="fa fa-edit "></i> User Profile</a>
                                        </td>
                                    </tr>
                               
                                    <?php
                                    $sn ++;
                                } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
               
            </div><!-- container -->

        </div> <!-- Page content Wrapper -->

    </div> <!-- content -->
</div>
 <script type="text/javascript">
   
   $('#form-user-create').submit(function(e){
        e.preventDefault();

        var xhr = submitForm2('#form-user-create');
        xhr.done(function(result){
            if(result.status){
               location.reload();
            }
        })
   });

   $('.a-user-delete').click(function(e){
        e.preventDefault();

        var a = $(this);
        if (confirm('Are you sure you want to delete this users?')) {

            var xhr = getRequest(a.attr('href'));
            xhr.done(function(result){
                if(result.status){
                    a.parent().parent().fadeOut(4000);
                }
            });
        } 
   })

</script>
<!-- End Right content here -->