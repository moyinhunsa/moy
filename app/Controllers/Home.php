<?php namespace App\Controllers;

use App\Models\AppModel;

class Home extends BaseController
{
	public function index()
	{
		$app_model = new AppModel();

		$data['posts']      = $app_model->postsList(3);
		$data['plans']      = $app_model->plansList();
		$data['banners']    = $app_model->bannerList();
		$data['about_us']   = $app_model->aboutUsData();
		$data['services']   = $app_model->servicesList();
		$data['site_info']  = $app_model->siteInfoData();
		$data['testimony']  = $app_model->testimonyList();
		$data['users']      = $app_model->usersList();

		echo view('home/header', $data);
		echo view('home/home', $data);
		echo view('home/footer');
	}

	public function about()
	{   
		$app_model = new AppModel();

		$data['site_info']  = $app_model->siteInfoData();
		echo view('home/header', $data);
		echo view("home/about");
		echo view('home/footer');
    }
	
	public function services()
	{   
		$app_model = new AppModel();

		$data['site_info']  = $app_model->siteInfoData();
		echo view('home/header', $data);
		echo view("home/services");
		echo view('home/footer');
    }
	
	public function blog()
	{   
		$app_model = new AppModel();

		$data['site_info']  = $app_model->siteInfoData();
		echo view('home/header', $data);
		echo view("home/blog");
		echo view('home/footer');
	}
	
	public function contact()
	{   
		$app_model = new AppModel();

		$data['site_info']  = $app_model->siteInfoData();
		echo view('home/header', $data);
		//echo view("home/services");
		echo view('home/footer');
    }
	//--------------------------------------------------------------------

}
