<?php namespace App\Controllers;

Class Admin extends BaseController {

    public $user;

    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);
        
        if(empty($this->user = $this->session->user)) die("login"); // User must be logged in.
       
    }

    public function index()
	{
        echo view("admin/header");
        echo view("admin/sidebar");
        echo view("admin/admin");
        echo view('admin/footer');
    }

    public function about(){
   
        $data['about_us'] = $this->app_model->aboutUsData(); 
        echo  view("admin/header");
        echo view("admin/sidebar");
        echo view("admin/about", $data);
        echo view('admin/footer');
    }
   
    public function about_edit_process(){

         $resp = ['status' => false, 'message' => '', 'data' => []];

        $data = $_POST;

        if(empty($data['title'])){
            $resp['message'] = 'About title is Required';

        } elseif(empty($data['short_desc'])){
            $resp['message'] = 'Short Description is Required';

        } elseif(empty($data['long_desc'])){
            $resp['message'] = 'long Description is Required';

        } elseif(empty($data['experience'])){
            $resp['message'] = 'Experience is Required';

        } else {

            if(!$this->app_model->aboutUpdate($data, $_FILES)){
                $resp['message'] = 'Something went wrong';
                
            } else{
                $resp['status'] = true;
                $resp['message'] = 'Updated Successfully';

                if(!empty($x = $_FILES['about_img']['name'])){

                $resp['data']['about_img'] = '../'.aboutUsImageSrc($x);
                }
            }
             
        }

        echo json_encode($resp);
    }

    public function banner_create(){

         $resp = ['status' => false, 'message' => '', 'data' => []];

        $data = $_POST;

        if(empty($data['banner_title'])){
            $resp['message'] = 'Banner title is Required';

        } elseif(empty($data['banner_price'])){
            $resp['message'] = 'Banner price is Required';

        } elseif(empty($_FILES['banner_img']['name'])){

            $resp['message'] = 'Banner image is Required';
        } else {

            if(!$this->app_model->bannerCreate($data, $_FILES)){
                $resp['message'] = 'Something went wrong';
                
            } else{
                $resp['status'] = true;
                $resp['message'] = 'Created Successfully';

            } 
            
        }

        echo json_encode($resp);

     }

     public function banner_delete(){

        $resp = ['status' => false, 'message' => '', 'data' => []];

        if(!$this->app_model->bannerDelete($_GET['banner_id'])){
            $resp['message'] = 'Something went wrong';
            
        }else{
            $resp['status'] = true;
            $resp['message'] = 'Deleted Successfully';
        }

        echo json_encode($resp);
            }

    public function banner_edit_process(){
        $resp = ['status' => false, 'message' => '', 'data' => []];

        $data = $_POST;

        if(empty($data['banner_title'])){
            $resp['message'] = 'Banner title is Required';

        } elseif(empty($data['banner_price'])){
            $resp['message'] = 'Banner price is Required';
        }else {

            if(!$this->app_model->bannerUpdate($data, $_FILES)){
                $resp['message'] = 'Something went wrong';
                
            }else{
                $resp['status'] = true;
                $resp['message'] = 'Updated Successfully';

                if(!empty($x = $_FILES['banner_img']['name'])){

                $resp['data']['banner_img'] = '../'.bannerImageSrc($x);
                }
            } 
            
        }

        echo json_encode($resp);

        }

    public function banner_edit(){

        $data['b'] = $this->app_model->bannerData($_GET['banner_id']);
        echo view('admin/header');
        echo view('admin/sidebar');
        echo view('admin/banner_edit', $data);
        echo view('admin/footer');
       
    }

    public function banner(){
        $data['banners'] = $this->app_model->bannerList();
        echo view('admin/header');
        echo view('admin/sidebar');
        echo view('admin/banner', $data);
        echo view('admin/footer');
    }

    public function change_password(){
        $data['u'] = $this->app_model->userData(['user_id' => $_GET['user_id']]);
        echo view('admin/header');
        echo view('admin/sidebar');
        echo view('admin/change_password', $data);
        echo view('admin/footer');
    }
    

    public function plan_create(){

        $resp = ['status' => false, 'message' => '', 'data' => []];

        $data = $_POST;

        if(empty($data['residence'])){
            $resp['message'] = 'Residence is Required';

        } elseif(empty($data['bed_bath'])){
            $resp['message'] = 'Bed Bath is Required';

        } elseif(empty($data['sqft'])){
            $resp['message'] = 'Sqft is Required';

        } elseif(empty($data['sale_price'])){
            $resp['message'] = 'Sale price is Required';
            
        } elseif(empty($data['rent_price'])){
            $resp['message'] = 'Rent price is Required';
        } else {

            if(!$this->app_model->planCreate($data, $_FILES)){
                $resp['message'] = 'Something went wrong';
                
            } else{
                $resp['status'] = true;
                $resp['message'] = 'Created Successfully';

            }
            
        }

        echo json_encode($resp);
    }

    public function plan_delete(){

        $resp = ['status' => false, 'message' => '', 'data' => []];

        if(!$this->app_model->planDelete($_GET['plan_id'])){
            $resp['message'] = 'Something went wrong';
            
        }else{
            $resp['status'] = true;
            $resp['message'] = 'Deleted Successfully';
        }

        echo json_encode($resp);

    }

    public function plan_edit_process(){

        $resp = ['status' => false, 'message' => '', 'data' => []];

        $data = $_POST;

        if(empty($data['residence'])){
            $resp['message'] = 'Residence is Required';

        } elseif(empty($data['bed_bath'])){
            $resp['message'] = 'Bed Bath is Required';

        } elseif(empty($data['sqft'])){
            $resp['message'] = 'Sqft is Required';

        } elseif(empty($data['sale_price'])){
            $resp['message'] = 'Sale price is Required';
            
        } elseif(empty($data['rent_price'])){
            $resp['message'] = 'Rent price is Required';
        }else {

            if(!$this->app_model->planUpdate($data, $_FILES)){
                $resp['message'] = 'Something went wrong';
                
            }else{
                $resp['status'] = true;
                $resp['message'] = 'Updated Successfully';

                if(!empty($x = $_FILES['plan_img']['name'])){

                $resp['data']['plan_img'] = '../'.planImageSrc($x);
                }
            }
                
        }

        echo json_encode($resp);
    }

    public function plan_edit(){

        $data['p'] = $this->app_model->planData($_GET['plan_id']);
        echo view('admin/header');
        echo view('admin/sidebar');
        echo view('admin/plan_edit',$data);
        echo view('admin/footer');
    }

    public function plans(){

        $data['plans'] = $this->app_model->plansList();
        echo view('admin/header');
        echo view('admin/sidebar');
        echo view('admin/plans',$data);
        echo view('admin/footer');
    }
    
    public function post_create(){
        $resp = ['status' => false, 'message' => '', 'data' => []];

        $data = $_POST;

        if(empty($data['post_title'])){
            $resp['message'] = 'post title is Required';

        } elseif(empty($data['post_details'])){
            $resp['message'] = 'post details is Required';

        } elseif(empty($_FILES['post_img']['name'])){

            $resp['message'] = 'Post image is Required';  
        } else {

            $data['created_by'] = $this->user['user_id'];

            if(!$this->app_model->postCreate($data, $_FILES)){
                $resp['message'] = 'Something went wrong';
                
            } else{
                $resp['status'] = true;
                $resp['message'] = 'Created Successfully';

            }
            
            
        }

        echo json_encode($resp);
    }

    public function post_delete($post_id){

        $resp = ['status' => false, 'message' => '', 'data' => []];

        if(!$this->app_model->postDelete($post_id)){
            $resp['message'] = 'Something went wrong';
            
        }else{
            $resp['status'] = true;
            $resp['message'] = 'Deleted Successfully';
        }

        echo json_encode($resp);
     }

     public function post_edit_process(){

        $resp = ['status' => false, 'message' => '', 'data' => []];

        $data = $_POST;

        if(empty($data['post_title'])){
            $resp['message'] = 'post title is Required';

        } elseif(empty($data['post_details'])){
            $resp['message'] = 'post details is Required';

        } else {

            if(!$this->app_model->postUpdate($data, $_FILES)){
                $resp['message'] = 'Something went wrong';
                
            }else{
                $resp['status'] = true;
                $resp['message'] = 'Updated Successfully';

                if(!empty($x = $_FILES['post_img']['name'])){

                $resp['data']['post_img'] = '../'.postImageSrc($x);
                }
            }
                
        }

        echo json_encode($resp);
     }

    public function post_edit($post_id){

        $data['p'] = $this->app_model->postData($post_id); 

        echo view('admin/header');
        echo view('admin/sidebar');
        echo view('admin/post_edit',$data);
        echo view('admin/footer');
     }

    public function posts(){
        $data['posts'] =$this->app_model->postsList(3);
        echo view('admin/header');
        echo view('admin/sidebar');
        echo view('admin/posts',$data);
        echo view('admin/footer');
     }

     public function service_create(){

        $resp = ['status' => false, 'message' => '', 'data' => []];

        $data = $_POST;

        if(empty($data['service_title'])){
            $resp['message'] = 'Service title is Required';

        } elseif(empty($data['service_icon'])){
            $resp['message'] = 'service icon is Required';

        } elseif(empty($data['service_desc'])){
            $resp['message'] = 'service desc is Required';

        } else {

            if(!$this->app_model->serviceCreate($data, $_FILES)){
                $resp['message'] = 'Something went wrong';
                
            } else{
                $resp['status'] = true;
                $resp['message'] = 'Created Successfully';

            }
            
        }

        echo json_encode($resp);
     }

     public function service_delete(){

        $resp = ['status' => false, 'message' => '', 'data' => []];

        if(!$this->app_model->serviceDelete($_GET['service_id'])){
            $resp['message'] = 'Something went wrong';
            
        }else{
            $resp['status'] = true;
            $resp['message'] = 'Deleted Successfully';
        }

        echo json_encode($resp);
     }

     public function service_edit_process(){

        $resp = ['status' => false, 'message' => '', 'data' => []];

        $data = $_POST;

        if(empty($data['service_title'])){
            $resp['message'] = 'Service title is Required';

        } elseif(empty($data['service_icon'])){
            $resp['message'] = 'Service icon is Required';

        } elseif(empty($data['service_desc'])){
            $resp['message'] = 'Service Description is Required';

        } else {

            if(!$this->app_model->serviceUpdate($data)){
                $resp['message'] = 'Something went wrong';
                
            }else{
                $resp['status'] = true;
                $resp['message'] = 'Updated Successfully';
            }
            
        }

        echo json_encode($resp);

     }

     public function service_edit(){

        $data['s'] =$this->app_model->serviceData($_GET['service_id']);
        echo view('admin/header');
        echo view('admin/sidebar');
        echo view('admin/service_edit',$data);
        echo view('admin/footer');
     }

     public function services(){

        $data['Services'] =$this->app_model->servicesList();
        echo view('admin/header');
        echo view('admin/sidebar');
        echo view('admin/services',$data);
        echo view('admin/footer');
     }

     public function site_edit_process(){
         
        $resp = ['status' => false, 'message' => '', 'data' => []];

        $data = $_POST;

        if(empty($data['site_email'])){
            $resp['message'] = 'Email is Required';

        } elseif(empty($data['site_phone'])){
            $resp['message'] = 'Phone Number is Required';

        } elseif(empty($data['site_address'])){
            $resp['message'] = 'Address is Required';

        } elseif(empty($data['site_working_hours'])){
            $resp['message'] = 'working hours is Required';

        }elseif(empty($data['footer_line'])){
            $resp['message'] = 'Footer Description is Required';

        } else {

            if(!$this->app_model->siteUpdate($data, $_FILES)){
                $resp['message'] = 'Something went wrong';
                
            } else{
                $resp['status'] = true;
                $resp['message'] = 'Updated Successfully';

                if(!empty($x = $_FILES['site_logo']['name'])){

                $resp['data']['site_logo'] = '../'.siteImageSrc($x);
                }
            }
            
        }

        echo json_encode($resp);

     }

     public function sitesetting(){

        $data['s'] =$this->app_model->siteinfoData();
        echo view('admin/header');
        echo view('admin/sidebar');
        echo view('admin/sitesetting',$data);
        echo view('admin/footer');

     }

     public function testimonial_create(){

        $resp = ['status' => false, 'message' => '', 'data' => []];

        $data = $_POST;
        
        if(empty($data['testimony'])){
            $resp['message'] = 'Testimony is Required';
        
        } elseif(empty($data['client_name'])){
            $resp['message'] = 'Client name is Required';
        
        } elseif(empty($data['client_desc'])){
            $resp['message'] = 'Client Description is Required';
        
        } elseif(empty($_FILES['client_img']['name'])){
        
            $resp['message'] = 'Client image is Required';
        } else {
        
            if(!$this->app_model->testimonyCreate($data, $_FILES)){
                $resp['message'] = 'Something went wrong';
                
            } else{
                $resp['status'] = true;
                $resp['message'] = 'Created Successfully';
        
            }   
            
        }
        
        echo json_encode($resp);
        
     }

     public function testimonial_delete(){
         
        $resp = ['status' => false, 'message' => '', 'data' => []];

        if(!$this->app_model->testimonyDelete($_GET['id'])){
            $resp['message'] = 'Something went wrong';
            
        }else{
            $resp['status'] = true;
            $resp['message'] = 'Deleted Successfully';
        }

        echo json_encode($resp);

     }

     public function testimonial_edit_process(){
         
        $resp = ['status' => false, 'message' => '', 'data' => []];

        $data = $_POST;

        if(empty($data['testimony'])){
            $resp['message'] = 'Testimony is Required';

        } elseif(empty($data['client_name'])){
            $resp['message'] = 'Client name is Required';

        } elseif(empty($data['client_desc'])){
            $resp['message'] = 'Client Description is Required';

        } else {

            if(!$this->app_model->testimonyUpdate($data, $_FILES)){
                $resp['message'] = 'Something went wrong';
                
            }else{
                $resp['status'] = true;
                $resp['message'] = 'Updated Successfully';

                if(!empty($x = $_FILES['client_img']['name'])){

                $resp['data']['client_img'] = '../'.clientImageSrc($x);
                }
            }
            
            
        }

        echo json_encode($resp);

     }

     public function testimonial_edit(){

        $data['t'] = $this->app_model-> testimonyData($_GET['id']);
        echo view('admin/header');
        echo view('admin/sidebar');
        echo view('admin/testimonial_edit',$data);
        echo view('admin/footer');

     }

     public function testimonials(){

        $data['testimony'] = $this->app_model->testimonyList();
        echo view('admin/header');
        echo view('admin/sidebar');
        echo view('admin/testimonials',$data);
        echo view('admin/footer');
    
     }
    
    public function user_create(){

        $resp = ['status' => false, 'message' => '', 'data' => []];

        $data = $_POST;

        if(empty($data['fullname'])){
            $resp['message'] = 'Name is Required';

        } elseif(empty($data['email'])){
            $resp['message'] = 'Email is Required';

        } elseif(empty($data['password'])){
            $resp['message'] = 'password is Required';

        } else {

            if(!$this->app_model->userCreate($data, $_FILES)){

                $resp['message'] = 'Something went wrong';
                
            } else{
                $resp['status'] = true;
                $resp['message'] = 'Created Successfully';

            }
            
        }

        echo json_encode($resp);

    }

    public function update_user(){

        $resp = ['status' => false, 'message' => '', 'data' => []];

        $data = $_POST;

        if(empty($data['fullname'])){
            $resp['message'] = 'Name is Required';

        } elseif(empty($data['email'])){
            $resp['message'] = 'Email is Required';

        } else {

            if(!$this->app_model->userUpdate($data, $_FILES)){
                $resp['message'] = 'Something went wrong';
                
            }else{
                $resp['status'] = true;
                $resp['message'] = 'Updated Successfully';

                if(!empty($x = $_FILES['photo']['name'])){

                $resp['data']['photo'] = userPhoto($x);
                }
            }
            
        }

        echo json_encode($resp);

     }

     public function user(){

        $data['users'] =$this->app_model->usersList();

        echo  view("admin/header");
        echo view("admin/sidebar");
        echo view("admin/user", $data);
        echo view('admin/footer');
    }

    public function user_profile(){

        $data['u'] = $this->app_model->userData(['user_id' => $_GET['user_id'] ?? $this->user['user_id']]);

        echo  view("admin/header");
        echo view("admin/sidebar");
        echo view("admin/user_profile", $data);
        echo view('admin/footer');
    }

    public function update_password(){

        $resp = ['status' => false, 'message' => '', 'data' => []];

        $data = $_POST;

        if(!password_verify($data['password'], $this->user['password'])){
            $resp['message'] = 'Incorrect current password';

        } elseif(empty($data['new_password'])){
            $resp['message'] = 'Your Password cannot be empty';

        } elseif($data['new_password'] !== $data['confirm_password']){
            $resp['message'] = 'Passwords do not match';
            
        } else{

            $update = [
                'password' => password_hash($data['new_password'], PASSWORD_DEFAULT), 
                'user_id'  => $this->user['user_id']
            ];

            if(!$this->app_model->passwordUpdate($update)){
                $resp['message'] = 'Something went wrong';

            } else{
                $resp['status'] = true;
                $resp['message'] = 'Created Successfully';

            }
        }
            

        echo json_encode($resp);
     }

}