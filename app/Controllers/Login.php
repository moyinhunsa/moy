<?php namespace App\Controllers;


class Login extends BaseController
{
	public $admin;

	public function __construct(){

		if(!empty($_SESSION['user'])) redirect("admin"); // User should not be here if logged in.

	}
	
	public function index()
	{
		echo view('login/login');

	}

	public function auth(){
		
		$resp = ['status' => false, 'message' => '', 'data' => []];

		$data = $_POST;

		if(empty($data['email'])){
			$resp['message'] = 'Email is required';

		} elseif(empty($user_data = $this->app_model->userData(['email' => $data['email']]))){
			$resp['message'] = 'That email does not exist';

		} elseif(!password_verify($data['password'], $user_data['password'])){
			$resp['message'] = 'Incorrect password';

		} else {

			$this->app_model->userLogin($user_data);

			$resp['status'] = true;
			$resp['message'] = 'Login Successful, Please Wait...';
			$resp['data']['goto'] = 'admin/index';
		}

		echo json_encode($resp);

	}


	public function logout(){
		$_SESSION = [];
		
		$this->response->redirect(site_url('login'));
    }

}
