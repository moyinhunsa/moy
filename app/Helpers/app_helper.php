<?php

use Config\Services;

function user($key = null){
	$user = $_SESSION['user'];
	return $key ? $user[$key] : $user;
}

function bannerImageSrc($banner_image){
	return base_url('assets/img/slider/'.$banner_image);
}

function aboutUsImageSrc($about_image){
	return base_url('assets/img/features/'.$about_image);
}

function clientImageSrc($client_image){
	return base_url('assets/img/testimonial/'.$client_image);
}

function postImageSrc($post_image){
	return base_url('assets/img/blog/'.$post_image);
}

function planImageSrc($plan_image){
	return base_url('assets/img/'.$plan_image);
}

function siteImageSrc($site_image){
	return base_url('assets/img/logo/'.$site_image);
}

function surround_with_quotes($x){
	return "'".addslashes($x)."'";
}

function reduce_text($z){
	return substr($z, 0, 50);
}
function userPhoto($user_image){
	return base_url('assets/images/users/'.$user_image);
}


?>