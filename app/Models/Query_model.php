<?php
namespace App\Models;

use CodeIgniter\Model;

Class Query_model extends Model
{

	public $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
	}
	
	public function singleRow($query){

		$result = $this->db->query($query);

		return $result->getRowArray();
	}
	
	
	public function multiRow($query){

		$result = $this->db->query($query);

		return $result->getResultArray();
	}	
	
}
        
      