<?php namespace App\Models;

use App\Models\Query_model;

class AppModel extends Query_model
{
    
    public function bannerList(){

        return $this->multiRow('SELECT * FROM banner');
    }
    
    public function bannerData($banner_id){
    
        return $this->singleRow("SELECT * FROM banner WHERE banner_id = '$banner_id'") ;
    }
    
    public function bannerUpdate($data, $files){
    
        if(!empty($files['banner_img']['name'])){
    
            if(!move_uploaded_file($files['banner_img']['tmp_name'], "assets/img/slider/{$files['banner_img']['name']}")) 
                return false;
                
                $data['banner_img'] = $files['banner_img']['name'];
        }

        return $this->db->table('banner')->update($data, "banner_id = {$data['banner_id']}");
    }
    
    public function bannerCreate($data, $files){
    
        if(!move_uploaded_file($files['banner_img']['tmp_name'], "assets/img/slider/{$files['banner_img']['name']}")) 
            return false;
            
        $data['banner_img'] = $files['banner_img']['name'];

        return $this->db->table('banner')->insert($data);
    }
    
    public function bannerDelete($banner_id){
    
        return $this->db->table('banner')->delete(['banner_id' => $banner_id]);
        
    }
    
    public function plansList(){
    
        return $this->multiRow("SELECT * FROM plans");
    }
    
    public function planData($plan_id){
    
        return $this->singleRow("SELECT * FROM plans WHERE plan_id = '$plan_id'") ;
    }
    
    public function planCreate($data, $files){
    
        if(!move_uploaded_file($files['plan_img']['tmp_name'], "assets/img/{$files['plan_img']['name']}")) 
            return false;
            
        $data['plan_img'] = $files['plan_img']['name'];
            
        return $this->db->table('plans')->insert($data);
    
    }
    
    public function planDelete($plan_id){
    
        return $this->db->table('plans')->delete(['plan_id' => $plan_id]);
    }
    
    public function planUpdate($data, $files){
    
            if(!empty($files['plan_img']['name'])){
    
            if(!move_uploaded_file($files['plan_img']['tmp_name'], "assets/img/slider/{$files['plan_img']['name']}")) 
                return false;
                
                $data['plan_img'] = $files['plan_img']['name'];
        }
        
        return $this->db->table('plans')->update($data, "plan_id = {$data['plan_id']}");
    }
    
    public function aboutUsData(){
    
       return $this-> singleRow("SELECT * FROM about_us");
    }
    
    public function aboutUpdate($data, $files){
    
            if(!empty($files['about_img']['name'])){
    
            if(!move_uploaded_file($files['about_img']['tmp_name'], "assets/img/features/{$files['about_img']['name']}")) 
                return false;
                
                $data['about_img'] = $files['about_img']['name'];
        }
        return $this->db->table('about_us')->update($data, "id =1");    
    
    }
    
    
    public function postsList($limit){
    
        return $this->multiRow("
			SELECT p.*, u.fullname 
			FROM posts p
			JOIN user u ON p.created_by = u.user_id
			LIMIT $limit"
		);
    }
    
    public function postData($post_id){
    
        return $this->singleRow("
			SELECT p.*, u.fullname FROM posts p
			JOIN user u ON p.created_by = u.user_id
			WHERE post_id = '$post_id'"
		) ;
    }
    
    public function postUpdate($data, $files){
    
            if(!empty($files['post_img']['name'])){
    
            if(!move_uploaded_file($files['post_img']['tmp_name'], "assets/img/blog/{$files['post_img']['name']}")) 
                return false;
                
                $data['post_img'] = $files['post_img']['name'];
        }
        return $this->db->table('posts')->update($data, "post_id = {$data['post_id']}");
    }
    
    public function postDelete($post_id){
    
        return $this->db->table('posts')->delete(['post_id' => $post_id]);
    }
    
    public function postCreate($data, $files){
    
        if(!move_uploaded_file($files['post_img']['tmp_name'], "assets/img/blog/{$files['post_img']['name']}")) 
            return false;
            
        $data['post_img'] = $files['post_img']['name'];
            
        return $this->db->table('posts')->insert($data);
    }
    
    public function servicesList(){
    
        return $this->multiRow("SELECT * FROM services");
    }
    
    public function serviceData($service_id){
    
        return $this->singleRow("SELECT * FROM services WHERE service_id = '$service_id'") ;
    }
    
    public function serviceUpdate($data){
    
        return $this->db->table('services')->update($data, "service_id = {$data['service_id']}");
    }
    
    public function serviceDelete($service_id){
    
        return $this->simpleQuery("DELETE FROM services WHERE service_id = '$service_id'") ;
        return $this->db->table('services')->delete(['service_id' => $service_id]);
        
    }
    
    public function serviceCreate($data){
    
        return $this->db->table('services')->insert($data);
    }
    
    public function testimonyList(){
    
        return $this->multiRow("SELECT * FROM testimony");
    }
    
    public function testimonyData($id){
    
        return $this->singleRow("SELECT * FROM testimony WHERE id = '$id'") ;
    }
    
    public function testimonyUpdate($data){
    
        return $this->db->table('testimony')->update($data, "id = {$data['id']}");
    }
    
    public function testimonyDelete($id){
    
        
        return $this->db->table('testimony')->delete(['id' => $id]);
    }
    
    public function testimonyCreate($data, $files){
    
        if(!move_uploaded_file($files['client_img']['tmp_name'], "assets/img/testimonial/{$files['client_img']['name']}")) 
            return false;
            
        $data['client_img'] = $files['client_img']['name'];
            
        return $this->db->table('testimony')->insert($data);
    }
    
    public function siteInfoData(){
    
        return $this->singleRow("SELECT * FROM site_info");
    }
    
    public function siteUpdate($data, $files){
    
            if(!empty($files['site_logo']['name'])){
    
            if(!move_uploaded_file($files['site_logo']['tmp_name'], "assets/logo/logo/{$files['site_logo']['name']}")) 
                return false;
                
                $data['site_logo'] = $files['site_logo']['name'];
        }
        return $this->db->table('site_info')->update($data, "id =1");
    
    }
    
    public function usersList(){
    
        return $this->multiRow("SELECT * FROM user");
    }
    
    public function userData($data){
    
        foreach ($data as $key => $value) {
            $clause[] = "$key = ".surround_with_quotes($value);
        }
    
        return $this->singleRow("SELECT * FROM user WHERE ".implode(' AND ', $clause));
    
    }
    
    public function userCreate($data, $files){
    
        if(!move_uploaded_file($files['photo']['tmp_name'],"assets/images/users/{$files['photo']['name']}")) 
            return false;
            
        $data['photo'] = $files['photo']['name'];
    
        $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
            
        return $this->db->table('user')->insert($data);
    }
    
    public function userUpdate($data, $files){
    
        if(!empty($files['photo']['name'])){
    
            if(!move_uploaded_file($files['photo']['tmp_name'], "assets/images/users/{$files['photo']['name']}")) 
                return false;
                
                $data['photo'] = $files['photo']['name'];
        }
        return $this->db->table('user')->update($data, "user_id = {$data['user_id']}");

    }

    public function passwordUpdate($data){

        return $this->db->table('user')->update($data, "user_id = {$data['user_id']}");
    }
    
    public function userLogin($data){
    
        $_SESSION['user'] = $data;
    
    }
   
}